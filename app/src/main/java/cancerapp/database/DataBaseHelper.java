package cancerapp.database;

import java.sql.SQLException;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

public class DataBaseHelper extends OrmLiteSqliteOpenHelper{
	private static final String DATABASE_NAME = "Cancerapp.db";
	private static final int DATABASE_VERSION = 1;
	private Dao<SelfExamAlert, String> selfexamDao = null;
	private Dao<GyaneAlert, String> gyaneDao = null;
	private Dao<MamoAlert, String> mamoDao = null;
	private Dao<RemindAlert, String> remindDao = null;
	public DataBaseHelper(Context context, String databaseName,
			CursorFactory factory, int databaseVersion) {
		super(context, databaseName, factory, databaseVersion);
		// TODO Auto-generated constructor stub
		
	}

	@Override
	public void onCreate(SQLiteDatabase sqliteDatabase, ConnectionSource connectionSource) {
		// TODO Auto-generated method stub
		try {
			TableUtils.createTable(connectionSource, SelfExamAlert.class);
			TableUtils.createTable(connectionSource, GyaneAlert.class);
			TableUtils.createTable(connectionSource, MamoAlert.class);
			TableUtils.createTable(connectionSource, RemindAlert.class);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase sqliteDatabase, ConnectionSource connectionSource, int oldVer,
			int newVer) {
		// TODO Auto-generated method stub
		try {
			TableUtils.dropTable(connectionSource, SelfExamAlert.class, true);
			TableUtils.dropTable(connectionSource, GyaneAlert.class, true);
			TableUtils.dropTable(connectionSource, MamoAlert.class, true);
			TableUtils.dropTable(connectionSource, RemindAlert.class, true);
			onCreate(sqliteDatabase, connectionSource);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
@SuppressWarnings("deprecation")
public Dao<SelfExamAlert,String>getSelfExamAlertDao() throws SQLException {
	if (selfexamDao == null) {
		selfexamDao = BaseDaoImpl.createDao(getConnectionSource(),
				SelfExamAlert.class);
	}
	return selfexamDao;	
}
@SuppressWarnings("deprecation")
public Dao<GyaneAlert,String>getGyaneAlertDao() throws SQLException{
	if(gyaneDao==null){
		gyaneDao=BaseDaoImpl.createDao(getConnectionSource(),GyaneAlert.class);
	}
	return gyaneDao;
}
@SuppressWarnings("deprecation")
public Dao<MamoAlert,String>getMamoAlertDao() throws SQLException{
	if(mamoDao==null){
		mamoDao=BaseDaoImpl.createDao(getConnectionSource(),MamoAlert.class);
	}
	return mamoDao;
}
@SuppressWarnings("deprecation")
public Dao<RemindAlert,String>getRemindAlertDao() throws SQLException{
	if(remindDao==null){
		remindDao=BaseDaoImpl.createDao(getConnectionSource(), RemindAlert.class);
	}
	return remindDao;
}
}
