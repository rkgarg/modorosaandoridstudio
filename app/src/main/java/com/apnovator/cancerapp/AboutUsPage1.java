package com.apnovator.cancerapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class AboutUsPage1 extends Activity {
	String abt=" ";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about_us_page1);
		Intent intent=getIntent();
		abt=intent.getStringExtra("name");
		WebView abtus1 = (WebView) findViewById(R.id.abtus1);
		abtus1.getSettings().setJavaScriptEnabled(true);
		final Activity activity = this;
		abtus1.setWebViewClient(new WebViewClient() {
			public void onReceivedError(WebView view, int errorCode,
					String description, String failingUrl) {
				Toast.makeText(activity, description, Toast.LENGTH_SHORT)
						.show();
			}
		});
if(abt.equals("link1")){
		abtus1.loadUrl("http://1tucan.com/portafolio-1tucan.com");
	}
else if(abt.equals("link2")){
	abtus1.loadUrl("http://www.enjoygram.com/365papel");
}
else if(abt.equals("link3")){
	abtus1.loadUrl("http://modorosa.com/impacto/");
}
else if(abt.equals("link4")){
	abtus1.loadUrl("http://modorosa.com/iluminacion-rosa/");
}
else if(abt.equals("link5")){
	abtus1.loadUrl("http://tripartitacomunicaciones.com");
	
}
else if(abt.equals("link6")){
	abtus1.loadUrl("http://modorosa.com/que-es-modo-rosa-2/");
	
}
else if(abt.equals("link7")){
	abtus1.loadUrl("http://modorosa.com");
	
}
	}
}

