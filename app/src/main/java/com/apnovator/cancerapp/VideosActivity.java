package com.apnovator.cancerapp;

import java.io.IOException;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

public class VideosActivity extends Activity {
	private MediaPlayer mMediaPlayer;
	ScrollView videoScroll;
	WebView webView1;
	WebView webView2;
	SharedPreferences preference;
	private String langSelection = " ";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		preference = getSharedPreferences("MyPREFERENCES",
				Context.MODE_WORLD_READABLE);
		langSelection = preference.getString("langSelect", " ");
		if (langSelection.compareTo("spanish") == 0) {
			Locale locale2 = new Locale("sp");
			Locale.setDefault(locale2);
			Configuration config = new Configuration();
			config.locale = locale2;
			getBaseContext().getResources().updateConfiguration(config,
					getBaseContext().getResources().getDisplayMetrics());
		} else if (langSelection.compareTo("english") == 0) {
			Locale locale2 = new Locale("en");
			Locale.setDefault(locale2);
			Configuration config = new Configuration();
			config.locale = locale2;
			getBaseContext().getResources().updateConfiguration(config,
					getBaseContext().getResources().getDisplayMetrics());
		}
		else if(langSelection.compareTo("french")==0){
			Locale locale2 = new Locale("fr"); 
	        Locale.setDefault(locale2);
				Configuration config = new Configuration();
				config.locale = locale2;
				getBaseContext().getResources().updateConfiguration(config,
				getBaseContext().getResources().getDisplayMetrics());
		}
		else if(langSelection.compareTo("purtuge")==0){
			Locale locale2 = new Locale("pt");
			Locale.setDefault(locale2);
			Configuration config = new Configuration();
			config.locale = locale2;
			getBaseContext().getResources().updateConfiguration(config,
					getBaseContext().getResources().getDisplayMetrics());
		}
		setContentView(R.layout.activity_videos);
		webView1 = (WebView) findViewById(R.id.webview1);
		webView2 = (WebView) findViewById(R.id.webview2);
		webView1.getSettings().setJavaScriptEnabled(true);
		//webView1.getSettings().setBuiltInZoomControls(true);

		//webView2.getSettings().setBuiltInZoomControls(true);

		webView2.getSettings().setJavaScriptEnabled(true);
		final Activity activity = this;

		ImageButton btn_Sound = (ImageButton) findViewById(R.id.btn_Sound1);
		Typeface type1 = Typeface.createFromAsset(getAssets(),
				"fonts/AvenirLTStd-Heavy.otf");
		Typeface type2 = Typeface.createFromAsset(getAssets(),
				"fonts/AvenirLTStd-Roman.otf");
		TextView videoText2 = (TextView) findViewById(R.id.videoText2);
		TextView videoText1 = (TextView) findViewById(R.id.videoText1);
		videoScroll = (ScrollView) findViewById(R.id.videoScroll);
		videoText1.setTypeface(type1);
		videoText2.setTypeface(type2);
		TextView videoText4 = (TextView) findViewById(R.id.videoText4);
		TextView videoText3 = (TextView) findViewById(R.id.videoText3);
		videoText3.setTypeface(type1);
		videoText4.setTypeface(type2);
		ImageButton downVideo = (ImageButton) findViewById(R.id.downVideo);
		downVideo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				videoScroll.scrollBy(0, 40);
			}
		});

		webView1.setWebChromeClient(new WebChromeClient() {
			public void onProgressChanged(WebView view, int progress) {

				activity.setTitle("Loading...");
				activity.setProgress(progress * 100);
				if (progress == 100)
					activity.setTitle(R.string.app_name);
			}
		});
		webView1.setWebViewClient(new WebViewClient() {
			public void onReceivedError(WebView view, int errorCode,
					String description, String failingUrl) {
				Toast.makeText(activity, description, Toast.LENGTH_SHORT)
						.show();
			}
		});
		webView2.setWebChromeClient(new WebChromeClient() {
			public void onProgressChanged(WebView view, int progress) {
				activity.setTitle("Loading...");
				activity.setProgress(progress * 100);
				if (progress == 100)
					activity.setTitle(R.string.app_name);
			}

		});
		webView2.setWebViewClient(new WebViewClient() {
			public void onReceivedError(WebView view, int errorCode,
					String description, String failingUrl) {
				Toast.makeText(activity, description, Toast.LENGTH_SHORT)
						.show();
			}
		});

		// webView1.loadUrl("https://www.youtube.com/watch?v=MDho7-NRuvA");
		// webView2.loadUrl("https://www.youtube.com/watch?v=gjB_0IUdqjA");
		final String mimeType = "text/html";
		final String encoding = "UTF-8";
		String html = getHTML("MDho7-NRuvA");
		String html1 = getHTML1("gjB_0IUdqjA");
		webView1.loadDataWithBaseURL("", html1, mimeType, encoding, "");
		webView2.loadDataWithBaseURL("", html, mimeType, encoding, "");
	}

	public String getHTML(String videoId) {

		String html = "<iframe class=\"youtube-player\" "
				+ "style=\"border: 0; width: 100%; height: 95%;"
				+ "padding:0px; margin:0px\" "
				+ "id=\"ytplayer\" type=\"text/html\" "
				+ "src=\"http://www.youtube.com/embed/" + videoId
				+ "?fs=1\" frameborder=\"0\" " + "allowfullscreen  autobuffer"
				+ "controls onclick=\"this.play()\">\n" + "</iframe>\n";

		/**
		 * <iframe id="ytplayer" type="text/html" width="640" height="360"
		 * src="https://www.youtube.com/embed/WM5HccvYYQg" frameborder="0"
		 * allowfullscreen>
		 **/

		return html;
	}

	public String getHTML1(String videoId) {

		String html = "<iframe class=\"youtube-player\" "
				+ "style=\"border: 0; width: 100%; height: 95%;"
				+ "padding:0px; margin:0px\" "
				+ "id=\"ytplayer\" type=\"text/html\" "
				+ "src=\"http://www.youtube.com/embed/" + videoId
				+ "?fs=0\" frameborder=\"0\" " + "allowfullscreen autobuffer "
				+ "controls onclick=\"this.play()\">\n" + "</iframe>\n";

		/**
		 * <iframe id="ytplayer" type="text/html" width="640" height="360"
		 * src="https://www.youtube.com/embed/WM5HccvYYQg" frameborder="0"
		 * allowfullscreen>
		 **/

		return html;
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		 super.onBackPressed();

		webView1.stopLoading();
		webView1.loadUrl("");
		// webView1.reload();
		// webView1 = null;
		webView2.stopLoading();
		webView2.loadUrl("");
		// webView2.reload();
		// webView2 = null;
	}
}