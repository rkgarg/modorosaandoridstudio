package com.apnovator.cancerapp;

import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class AboutUsActivity extends Activity {
	String about = " ";
	ImageButton abtusbtn1;
	ImageButton abtusbtn2;
	SharedPreferences preference;
	private String langSelection=" ";
	// TextView abtUsText1;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		preference  = getSharedPreferences("MyPREFERENCES",Context.MODE_WORLD_READABLE);
		langSelection=preference.getString("langSelect"," ");
		if(langSelection.compareTo("spanish")==0){
		Locale locale2 = new Locale("sp"); 
        Locale.setDefault(locale2);
			Configuration config = new Configuration();
			config.locale = locale2;
			getBaseContext().getResources().updateConfiguration(config,
			getBaseContext().getResources().getDisplayMetrics());
		}
		else if(langSelection.compareTo("english")==0){
			Locale locale2 = new Locale("en"); 
	        Locale.setDefault(locale2);
				Configuration config = new Configuration();
				config.locale = locale2;
				getBaseContext().getResources().updateConfiguration(config,
				getBaseContext().getResources().getDisplayMetrics());
		}
		else if(langSelection.compareTo("french")==0){
			Locale locale2 = new Locale("fr");
	        Locale.setDefault(locale2);
				Configuration config = new Configuration();
				config.locale = locale2;
				getBaseContext().getResources().updateConfiguration(config,
				getBaseContext().getResources().getDisplayMetrics());
		}
		else if(langSelection.compareTo("purtuge")==0){
			Locale locale2 = new Locale("pt");
			Locale.setDefault(locale2);
			Configuration config = new Configuration();
			config.locale = locale2;
			getBaseContext().getResources().updateConfiguration(config,
					getBaseContext().getResources().getDisplayMetrics());
		}
		setContentView(R.layout.activity_about_us);
		abtusbtn1 = (ImageButton) findViewById(R.id.abtBtn1);
		abtusbtn2 = (ImageButton) findViewById(R.id.abtBtn2);
		 TextView abtUsText1=(TextView)findViewById(R.id.abtUsText1);
		TextView abtUsText2 = (TextView) findViewById(R.id.abtUsText2);
		 TextView abtUsText3=(TextView)findViewById(R.id.abtUsText3);
		TextView abtUsText4 = (TextView) findViewById(R.id.abtUsText4);
		//gv = new GraphicsView(this);
		Typeface type2 = Typeface.createFromAsset(getAssets(),
				"fonts/AvenirLTStd-Roman.otf");
		final RelativeLayout layout_pt3 = (RelativeLayout) findViewById(R.id.layout_pt3);
		final LinearLayout layout_pt4 = (LinearLayout) findViewById(R.id.layout_pt4);
		//layout_pt4.addView(gv);
		abtUsText1.setTypeface(type2);
		abtUsText2.setTypeface(type2);
		 abtUsText3.setTypeface(type2);
		abtUsText4.setTypeface(type2);

		abtusbtn1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				about = "link1";
				Intent intent1 = new Intent(AboutUsActivity.this,
						AboutUsPage1.class);
				intent1.putExtra("name", about);
				startActivity(intent1);

			}
		});
		abtusbtn2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				about = "link2";
				Intent intent2 = new Intent(AboutUsActivity.this,
						AboutUsPage1.class);
				intent2.putExtra("name", about);
				startActivity(intent2);

			}
		});

	/*	ViewTreeObserver vto = abtusbtn1.getViewTreeObserver();
		vto.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {

			@Override
			public void onGlobalLayout() {
				// TODO Auto-generated method stub
				abtusbtn1.getViewTreeObserver().removeGlobalOnLayoutListener(
						this);
				height_btn1 = abtusbtn1.getMeasuredHeight();
				width_btn1 = abtusbtn1.getMeasuredWidth();
				Log.v("height", " " + height_btn1);
			}

		});
		ViewTreeObserver vto1 = layout_pt4.getViewTreeObserver();
		vto1.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {

			@Override
			public void onGlobalLayout() {
				// TODO Auto-generated method stub
				layout_pt4.getViewTreeObserver().removeGlobalOnLayoutListener(
						this);
				height_l4 = layout_pt4.getMeasuredHeight();
				width_l4 = layout_pt4.getMeasuredWidth();
			}

		});
		ViewTreeObserver vto2 = layout_pt3.getViewTreeObserver();
		vto2.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {

			@Override
			public void onGlobalLayout() {
				// TODO Auto-generated method stub
				layout_pt3.getViewTreeObserver().removeGlobalOnLayoutListener(
						this);
				height_l3 = layout_pt3.getMeasuredHeight();
				width_l3 = layout_pt3.getMeasuredWidth();

			}

		});*/

	}

	/*public class GraphicsView extends View {
		Path path;
		Paint paint;
		String myText;

		public GraphicsView(Context context) {
			super(context);
			// TODO Auto-generated constructor stub
			myText = getResources().getString(R.string.aboutusTxt1);
			// Canvas canvas = new Canvas();
			path = new Path();
			paint = new Paint();
			Log.v("height", " " + height_btn1);
			Log.v("width1", " " + width_btn1);
			Log.v("height2", " " + height_l3);
			Log.v("height3", " " + width_l3);
			Log.v("height4", " " + height_l4);
			Log.v("height5", " " + width_l4);
			paint.setColor(Color.BLACK);
			paint.setTextSize(20);
	
			path.moveTo(width_btn1, height_l3);
		path.lineTo(width_btn1, height_btn1);
			path.lineTo(0, (height_btn1 + height_l3));
			path.lineTo(0, (height_l4 + height_l3));
			path.lineTo(width_l4, (height_l4 + height_l3));
			path.lineTo(width_l4, height_l3);
			//path.moveTo(300, 50);
			//path.lineTo(50, 50);
			//path.lineTo(0, (50 + 300));
			//path.lineTo(0, (300 + 50));
			//path.lineTo(300, (300 + 50));
			//path.lineTo(300, 50);

		}

		@Override
		protected void onDraw(Canvas canvas) {
			canvas.drawPath(path, paint);
			canvas.drawTextOnPath(myText, path, 0, 0, paint);
		}
	}*/

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		// Intent in=new Intent(this,HomeMenu.class);
		// startActivity(in);
		// finish();
	}
}
