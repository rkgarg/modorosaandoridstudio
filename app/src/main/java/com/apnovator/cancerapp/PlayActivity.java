package com.apnovator.cancerapp;

import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.MediaController;
import android.widget.VideoView;

public class PlayActivity extends Activity {
	String uri = " ";
	private int timevideo;
	VideoView videoView1;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_play);
		SharedPreferences preference = getSharedPreferences("MyPREFERENCES",
				Context.MODE_WORLD_READABLE);
		String langSelection = preference.getString("langSelect", " ");
		if (langSelection.compareTo("spanish") == 0) {
			Locale locale2 = new Locale("sp");
			Locale.setDefault(locale2);
			Configuration config = new Configuration();
			config.locale = locale2;
			getBaseContext().getResources().updateConfiguration(config,
					getBaseContext().getResources().getDisplayMetrics());
			uri = "android.resource://" + getPackageName() + "/"
					+ R.raw.video_autoexam1;
		} else if (langSelection.compareTo("english") == 0) {
			Locale locale2 = new Locale("en");
			Locale.setDefault(locale2);
			Configuration config = new Configuration();
			config.locale = locale2;
			getBaseContext().getResources().updateConfiguration(config,
					getBaseContext().getResources().getDisplayMetrics());
			uri = "android.resource://" + getPackageName() + "/"
					+ R.raw.autoexam2;
		}
		else if(langSelection.compareTo("french")==0){
			Locale locale2 = new Locale("fr"); 
	        Locale.setDefault(locale2);
				Configuration config = new Configuration();
				config.locale = locale2;
				getBaseContext().getResources().updateConfiguration(config,
				getBaseContext().getResources().getDisplayMetrics());
		}
		else if(langSelection.compareTo("purtuge")==0){
			Locale locale2 = new Locale("pt");
			Locale.setDefault(locale2);
			Configuration config = new Configuration();
			config.locale = locale2;
			getBaseContext().getResources().updateConfiguration(config,
					getBaseContext().getResources().getDisplayMetrics());
		}
	 videoView1 = (VideoView) findViewById(R.id.videoViewRelative);
		MediaController mc1 = new MediaController(this);
		videoView1.setMediaController(mc1);
		Bundle returnData = (Bundle) getLastNonConfigurationInstance();
   
		if (returnData == null) {
            String url = uri;
            videoView1.setVideoPath(url);
            videoView1.requestFocus();
            videoView1.start();
        } else {
            String url = uri;
            videoView1.setVideoPath(url);
            timevideo  = returnData.getInt("POSVIDEO");
            videoView1.seekTo(timevideo);
            videoView1.requestFocus();
            videoView1.start();

	
		

	//	videoView1.setVideoURI(Uri.parse(uri));
	//	videoView1.start();
	}
}

	@Override
    public Object onRetainNonConfigurationInstance() {

		// TODO Auto-generated method stub
		
		Bundle data = new Bundle();
        timevideo = videoView1.getCurrentPosition();
        data.putInt("POSVIDEO", timevideo);
        return data;
	}
	}
