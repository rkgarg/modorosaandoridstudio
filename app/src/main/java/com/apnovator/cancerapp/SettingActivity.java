package com.apnovator.cancerapp;

import java.util.Locale;

import android.app.Activity;
import android.app.ActionBar.LayoutParams;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

public class SettingActivity extends Activity {
String Setting="Setting";
Button btnClosePopup1;
private PopupWindow pwindo1;
SharedPreferences preference;
private String langSelection=" ";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		preference  = getSharedPreferences("MyPREFERENCES",Context.MODE_WORLD_READABLE);
		langSelection=preference.getString("langSelect"," ");
		if(langSelection.compareTo("spanish")==0){
		Locale locale2 = new Locale("sp"); 
        Locale.setDefault(locale2);
			Configuration config = new Configuration();
			config.locale = locale2;
			getBaseContext().getResources().updateConfiguration(config,
			getBaseContext().getResources().getDisplayMetrics());
		}
		else if(langSelection.compareTo("english")==0){
			Locale locale2 = new Locale("en"); 
	        Locale.setDefault(locale2);
				Configuration config = new Configuration();
				config.locale = locale2;
				getBaseContext().getResources().updateConfiguration(config,
				getBaseContext().getResources().getDisplayMetrics());
		}
		else if(langSelection.compareTo("french")==0){
			Locale locale2 = new Locale("fr"); 
	        Locale.setDefault(locale2);
				Configuration config = new Configuration();
				config.locale = locale2;
				getBaseContext().getResources().updateConfiguration(config,
				getBaseContext().getResources().getDisplayMetrics());
		}
		else if(langSelection.compareTo("purtuge")==0){
			Locale locale2 = new Locale("pt");
			Locale.setDefault(locale2);
			Configuration config = new Configuration();
			config.locale = locale2;
			getBaseContext().getResources().updateConfiguration(config,
					getBaseContext().getResources().getDisplayMetrics());
		}
		setContentView(R.layout.activity_setting);
		ImageView moreinfo=(ImageView)findViewById(R.id.moreinfo);
		Typeface type1=Typeface.createFromAsset(getAssets(),"fonts/AvenirLTStd-Heavy.otf");
		TextView consultText = (TextView) findViewById(R.id.consultText);
		consultText.setTypeface(type1);
		TextView change_lang_text = (TextView) findViewById(R.id.change_lang_text);
		change_lang_text.setTypeface(type1);
		/*moreinfo.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				LayoutInflater inflater = (LayoutInflater) SettingActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				View layout = inflater.inflate(R.layout.moreinfo_popup,(ViewGroup) findViewById(R.id.popup_element2));
			
				pwindo1 = new PopupWindow(layout,LayoutParams.FILL_PARENT,LayoutParams.FILL_PARENT,true);
				pwindo1.showAtLocation(layout, Gravity.CENTER, 0, 0);
				btnClosePopup1 = (Button) layout.findViewById(R.id.btn_close_popup1);
				btnClosePopup1.setOnClickListener(cancel_button_click_listener);
			}
		});
	*/
		
		ImageView sponsor=(ImageView)findViewById(R.id.sponcer);
		/*sponsor.setOnClickListener(new OnClickListener(){
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				LayoutInflater inflater = (LayoutInflater) SettingActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				View layout = inflater.inflate(R.layout.sponser_popup,(ViewGroup) findViewById(R.id.popup_element2));
				pwindo1 = new PopupWindow(layout,LayoutParams.FILL_PARENT,LayoutParams.FILL_PARENT,true);
				pwindo1.showAtLocation(layout, Gravity.CENTER, 0, 0);
				btnClosePopup1 = (Button) layout.findViewById(R.id.btn_close_popup1);
				btnClosePopup1.setOnClickListener(cancel_button_click_listener1);
				
				
			}
		});*/
		ImageView changelang=(ImageView)findViewById(R.id.changeLang);
		changelang.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String msg="Setting";
				Editor ed = preference.edit();
				ed.putBoolean("activity_executed", false);
				ed.putString(Setting, "Setting");
				ed.commit();
				Intent intent=new Intent(SettingActivity.this,LangSelection.class);
				intent.putExtra("caller",msg );
                startActivity(intent);
                finish();
			}
		});
		ImageView aboutus=(ImageView)findViewById(R.id.aboutus);
		aboutus.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent=new Intent(SettingActivity.this,AboutUsActivity.class);
				startActivity(intent);	
				
			}
		});
	}
	private OnClickListener cancel_button_click_listener1 = new OnClickListener() {
		public void onClick(View v) {
			pwindo1.dismiss();
			Intent intent=new Intent(SettingActivity.this,SponsorActivity.class);
			startActivity(intent);
			
		}
	};
	private OnClickListener cancel_button_click_listener = new OnClickListener() {
		public void onClick(View v) {
			pwindo1.dismiss();
			Intent intent = new Intent(SettingActivity.this,MoreInfoActivity.class);
			startActivity(intent);
			
		}
	};
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		Intent in=new Intent(this,HomeMenu.class);
		startActivity(in);
		finish();
	}
}

