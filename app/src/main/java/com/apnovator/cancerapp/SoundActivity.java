package com.apnovator.cancerapp;

import java.io.IOException;
import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;

public class SoundActivity extends Activity {
	private MediaPlayer mMediaPlayer;
	private int id;
	int alarmId;
	SharedPreferences preference;
	private String langSelection=" ";
	ImageView sound_icon1;
	ImageView sound_icon2;
	ImageView sound_icon3;
	ImageView sound_icon4;
	ImageView sound_icon5;
	ImageView sound_icon6;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		preference  = getSharedPreferences("MyPREFERENCES",Context.MODE_WORLD_READABLE);
		langSelection=preference.getString("langSelect"," ");
		if(langSelection.compareTo("spanish")==0){
		Locale locale2 = new Locale("sp"); 
        Locale.setDefault(locale2);
			Configuration config = new Configuration();
			config.locale = locale2;
			getBaseContext().getResources().updateConfiguration(config,
			getBaseContext().getResources().getDisplayMetrics());
		}
		else if(langSelection.compareTo("english")==0){
			Locale locale2 = new Locale("en"); 
	        Locale.setDefault(locale2);
				Configuration config = new Configuration();
				config.locale = locale2;
				getBaseContext().getResources().updateConfiguration(config,
				getBaseContext().getResources().getDisplayMetrics());
		}
		else if(langSelection.compareTo("french")==0){
			Locale locale2 = new Locale("fr"); 
	        Locale.setDefault(locale2);
				Configuration config = new Configuration();
				config.locale = locale2;
				getBaseContext().getResources().updateConfiguration(config,
				getBaseContext().getResources().getDisplayMetrics());
		}
		else if(langSelection.compareTo("purtuge")==0){
			Locale locale2 = new Locale("pt");
			Locale.setDefault(locale2);
			Configuration config = new Configuration();
			config.locale = locale2;
			getBaseContext().getResources().updateConfiguration(config,
					getBaseContext().getResources().getDisplayMetrics());
		}
		setContentView(R.layout.activity_sound);
		Intent intent=getIntent();
		alarmId=intent.getIntExtra("AlarmId", 0);
		Log.v("alarm"," "+alarmId);
		//preference = getSharedPreferences("ActivityPREF", Context.MODE_PRIVATE);
	    Typeface type2=Typeface.createFromAsset(getAssets(),"fonts/AvenirLTStd-Roman.otf");
		Button sound1 = (Button) findViewById(R.id.sound1);
		Button sound2 = (Button) findViewById(R.id.sound2);
		Button sound3 = (Button) findViewById(R.id.sound3);
		//Button sound4 = (Button) findViewById(R.id.sound4);
		Button sound5 = (Button) findViewById(R.id.sound5);
		Button sound6 = (Button) findViewById(R.id.sound6);
		sound1.setTypeface(type2);
		sound2.setTypeface(type2);
		sound3.setTypeface(type2);
		sound5.setTypeface(type2);
		sound6.setTypeface(type2);
	 sound_icon1=(ImageView)findViewById(R.id.sound_icon1);
		sound_icon2=(ImageView)findViewById(R.id.sound_icon2);
		sound_icon3=(ImageView)findViewById(R.id.sound_icon3);
		//sound_icon4=(ImageView)findViewById(R.id.sound_icon4);
		sound_icon5=(ImageView)findViewById(R.id.sound_icon5);
		sound_icon6=(ImageView)findViewById(R.id.sound_icon6);
		sound1.setOnClickListener(myHandler);
		sound2.setOnClickListener(myHandler);
		sound3.setOnClickListener(myHandler);
		//sound4.setOnClickListener(myHandler);
		sound5.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				id=5;
				sound_icon1.setVisibility(View.GONE);
				sound_icon2.setVisibility(View.GONE);
				sound_icon3.setVisibility(View.GONE);
				//sound_icon4.setVisibility(View.GONE);
				sound_icon5.setVisibility(View.VISIBLE);
				sound_icon6.setVisibility(View.GONE);
				if (mMediaPlayer != null && mMediaPlayer.isPlaying()) {
					mMediaPlayer.stop();
			        //mMediaPlayer.release();
					}
			}
		});
		sound6.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				id=6;
				// TODO Auto-generated method stub
				sound_icon1.setVisibility(View.GONE);
				sound_icon2.setVisibility(View.GONE);
				sound_icon3.setVisibility(View.GONE);
				//sound_icon4.setVisibility(View.GONE);
				sound_icon5.setVisibility(View.GONE);
				sound_icon6.setVisibility(View.VISIBLE);
				if (mMediaPlayer != null && mMediaPlayer.isPlaying()) {
					mMediaPlayer.stop();
					//mMediaPlayer.release();
					}
				Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
				vibrator.vibrate(2000);
			}
		});
	}

	View.OnClickListener myHandler = new View.OnClickListener() {
		public void onClick(View v) {
			if (mMediaPlayer != null && mMediaPlayer.isPlaying()) {
				mMediaPlayer.stop();
				//mMediaPlayer.release();
			}
			switch (v.getId()) {
			case R.id.sound1:
				sound_icon1.setVisibility(View.VISIBLE);
				sound_icon2.setVisibility(View.GONE);
				sound_icon3.setVisibility(View.GONE);
				//sound_icon4.setVisibility(View.GONE);
				sound_icon5.setVisibility(View.GONE);
				sound_icon6.setVisibility(View.GONE);
				id=1;
				mMediaPlayer = MediaPlayer.create(SoundActivity.this,
						R.raw.sound1);
				break;
			case R.id.sound2:
				sound_icon1.setVisibility(View.GONE);
				sound_icon2.setVisibility(View.VISIBLE);
				sound_icon3.setVisibility(View.GONE);
				//sound_icon4.setVisibility(View.GONE);
				sound_icon5.setVisibility(View.GONE);
				sound_icon6.setVisibility(View.GONE);
				id=2;
				mMediaPlayer = MediaPlayer.create(SoundActivity.this,
						R.raw.sound2);
				break;
			case R.id.sound3:
				id=3;
				sound_icon1.setVisibility(View.GONE);
				sound_icon2.setVisibility(View.GONE);
				sound_icon3.setVisibility(View.VISIBLE);
				//sound_icon4.setVisibility(View.GONE);
				sound_icon5.setVisibility(View.GONE);
				sound_icon6.setVisibility(View.GONE);
				mMediaPlayer = MediaPlayer.create(SoundActivity.this,
						R.raw.sound3);
				break;
		
			}

			try {
				mMediaPlayer.prepare();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			mMediaPlayer.start();
		}
	};

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		if (mMediaPlayer != null && mMediaPlayer.isPlaying()) {
		mMediaPlayer.stop();
		mMediaPlayer.release();
		}
		Editor editor = preference.edit();
		if(alarmId==1){
		editor.putInt("sound1",id);
		}
		else if(alarmId==2){
			editor.putInt("sound2",id);	
		}
		else if(alarmId==3){
			editor.putInt("sound3",id);	
		}
		else if(alarmId==4){
			editor.putInt("sound4",id);	
		}
		editor.commit();
		Intent returnIntent = new Intent();
		returnIntent.putExtra("sound_path", id);
		setResult(RESULT_OK, returnIntent);
		finish();
	}
};
