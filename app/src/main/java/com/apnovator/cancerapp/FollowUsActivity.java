package com.apnovator.cancerapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;

public class FollowUsActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_follow_us);
		ImageButton fb_btn=(ImageButton)findViewById(R.id.fb_btn);
		ImageButton instagram_btn=(ImageButton)findViewById(R.id.instagram_btn);
		ImageButton twitter_btn=(ImageButton)findViewById(R.id.twitter_btn);
		fb_btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent=new Intent(FollowUsActivity.this,AboutUsPage2.class);
				intent.putExtra("url", 1);
				startActivity(intent);
			}
		});
		instagram_btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent=new Intent(FollowUsActivity.this,AboutUsPage2.class);
				intent.putExtra("url", 2);
				startActivity(intent);
			}
		});
		twitter_btn.setOnClickListener(new OnClickListener() {
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		Intent intent=new Intent(FollowUsActivity.this,AboutUsPage2.class);
		intent.putExtra("url", 3);
		startActivity(intent);
	}
});
	}
}
