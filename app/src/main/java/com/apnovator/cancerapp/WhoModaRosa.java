package com.apnovator.cancerapp;

import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.TextView;

public class WhoModaRosa extends Activity {
	ScrollView sv;
	SharedPreferences preference;
	private String langSelection=" ";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		preference  = getSharedPreferences("MyPREFERENCES",Context.MODE_WORLD_READABLE);
		langSelection=preference.getString("langSelect"," ");
		if(langSelection.compareTo("spanish")==0){
		Locale locale2 = new Locale("sp"); 
        Locale.setDefault(locale2);
			Configuration config = new Configuration();
			config.locale = locale2;
			getBaseContext().getResources().updateConfiguration(config,
			getBaseContext().getResources().getDisplayMetrics());
		}
		else if(langSelection.compareTo("english")==0){
			Locale locale2 = new Locale("en"); 
	        Locale.setDefault(locale2);
				Configuration config = new Configuration();
				config.locale = locale2;
				getBaseContext().getResources().updateConfiguration(config,
				getBaseContext().getResources().getDisplayMetrics());
		}
		else if(langSelection.compareTo("french")==0){
			Locale locale2 = new Locale("fr"); 
	        Locale.setDefault(locale2);
				Configuration config = new Configuration();
				config.locale = locale2;
				getBaseContext().getResources().updateConfiguration(config,
				getBaseContext().getResources().getDisplayMetrics());
		}
		else if(langSelection.compareTo("purtuge")==0){
			Locale locale2 = new Locale("pt");
			Locale.setDefault(locale2);
			Configuration config = new Configuration();
			config.locale = locale2;
			getBaseContext().getResources().updateConfiguration(config,
					getBaseContext().getResources().getDisplayMetrics());
		}
		setContentView(R.layout.activity_who_moda_rosa);
		// final ScrollView sv1=(ScrollView)findViewById(R.id.sv1);
		final TextView who_text = (TextView) findViewById(R.id.who_text);
		ImageButton whomoda_btn = (ImageButton) findViewById(R.id.whomoda_btn);
		sv = (ScrollView) findViewById(R.id.sv1);
		Typeface type2=Typeface.createFromAsset(getAssets(),"fonts/AvenirLTStd-Roman.otf");
		who_text.setTypeface(type2);
		//who_text.setMovementMethod(new ScrollingMovementMethod());
		whomoda_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				sv.scrollBy(0, 60);
			}
		});
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		//Intent in = new Intent(this, HomeMenu.class);
		//startActivity(in);
		//finish();
	}
}
