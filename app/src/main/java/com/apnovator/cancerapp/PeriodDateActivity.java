package com.apnovator.cancerapp;

import java.util.Calendar;
import java.util.Locale;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

public class PeriodDateActivity extends Activity {
	private int mYear;
	private int mMonth;
	private int mDay;
	static final int DATE_DIALOG_ID = 0;
	private TextView dateEdittext1;
	private String dateStr = " ";
	private int cycleStr;
	SharedPreferences preference;
	private String langSelection = " ";
	public static final String keyCycleLength = "keyCycleLength";
	public static final String keySelf = "keySelf";
	String alarmType = "alarmType";
	String DefaultDate;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		preference = getSharedPreferences("MyPREFERENCES",
				Context.MODE_WORLD_READABLE);
		langSelection = preference.getString("langSelect", " ");
		if (langSelection.compareTo("spanish") == 0) {
			Locale locale2 = new Locale("sp");
			Locale.setDefault(locale2);
			Configuration config = new Configuration();
			config.locale = locale2;
			getBaseContext().getResources().updateConfiguration(config,
					getBaseContext().getResources().getDisplayMetrics());
		} else if (langSelection.compareTo("english") == 0) {
			Locale locale2 = new Locale("en");
			Locale.setDefault(locale2);
			Configuration config = new Configuration();
			config.locale = locale2;
			getBaseContext().getResources().updateConfiguration(config,
					getBaseContext().getResources().getDisplayMetrics());
		}
		else if(langSelection.compareTo("french")==0){
			Locale locale2 = new Locale("fr"); 
	        Locale.setDefault(locale2);
				Configuration config = new Configuration();
				config.locale = locale2;
				getBaseContext().getResources().updateConfiguration(config,
				getBaseContext().getResources().getDisplayMetrics());
		}
		else if(langSelection.compareTo("purtuge")==0){
			Locale locale2 = new Locale("pt");
			Locale.setDefault(locale2);
			Configuration config = new Configuration();
			config.locale = locale2;
			getBaseContext().getResources().updateConfiguration(config,
					getBaseContext().getResources().getDisplayMetrics());
		}
		setContentView(R.layout.activity_period_date);
		Intent intent1 = getIntent();
		final int m = intent1.getIntExtra("n", 0);
		TextView dateTextview1 = (TextView) findViewById(R.id.dateTextview1);

		Typeface type2 = Typeface.createFromAsset(getAssets(),
				"fonts/AvenirLTStd-Roman.otf");
		dateEdittext1 = (TextView) findViewById(R.id.dateEdittext1);
		dateEdittext1.setTypeface(type2);
		dateTextview1.setTypeface(type2);
		if (preference.getString(keySelf, " ").compareTo(" ") == 0) {
			setDefaultDate();
		} else {
			dateEdittext1.setText(preference.getString(keySelf, " "));
		}
		TextView dateTextView2 = (TextView) findViewById(R.id.dateTextview2);
		dateTextView2.setTypeface(type2);
		final Spinner s = (Spinner) findViewById(R.id.cycleSpinner);
		s.setSelection(preference.getInt("keyCycleLength", -1));

		if (m == 2) {
			dateTextView2.setVisibility(View.GONE);
			s.setVisibility(View.GONE);
			String simpleDate = getResources().getString(R.string.simpleDate);
			dateTextview1.setText(simpleDate);

		}

		ArrayAdapter adapter = ArrayAdapter.createFromResource(this,
				R.array.cycle, android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		s.setAdapter(adapter);
		s.setSelection(preference.getInt("keyCycleLength", 0));
		s.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int arg, long id) {
				// TODO Auto-generated method stub
				int position = (int) id;
				cycleStr = Integer.parseInt(parent.getItemAtPosition(position)
						.toString());
				Editor editor1 = preference.edit();
				editor1.putInt("keyCycleLength", position);
				Log.v("cycleSave",
						" " + preference.getInt("keyCycleLength", -1));
				Log.v("cycleSave1", " " + cycleStr);
				editor1.commit();
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}
		});

		Button bck_btn = (Button) findViewById(R.id.bck_btn1);
		bck_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// onBackPressed();
				Editor editor1 = preference.edit();
				if (m == 1) {
					editor1.putInt("alarmType", 1);
				} else {
					editor1.putInt("alarmType", 2);
				}
				Log.v("cycleSave", " " + preference.getInt("alarmType", -1));
				Log.v("cycleSave1", " " + cycleStr);
				editor1.commit();

				Intent returnIntent = new Intent();
				returnIntent.putExtra("selfDate", dateEdittext1.getText()
						.toString());
				returnIntent.putExtra("cycle", cycleStr);

				setResult(RESULT_OK, returnIntent);
				finish();
			}
		});
		ImageButton DateButton = (ImageButton) findViewById(R.id.dateButton1);

		DateButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Editor editor1 = preference.edit();
				if (m == 1) {
					editor1.putInt("alarmType", 1);
				} else {
					editor1.putInt("alarmType", 2);
				}
				Log.v("cycleSave", " " + preference.getInt("alarmType", -1));
				Log.v("cycleSave1", " " + cycleStr);
				editor1.commit();

				Intent returnIntent = new Intent();
				returnIntent.putExtra("selfDate", dateEdittext1.getText()
						.toString());
				returnIntent.putExtra("cycle", cycleStr);

				setResult(RESULT_OK, returnIntent);
				finish();

			}
		});

		dateEdittext1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showDialog(DATE_DIALOG_ID);
			}
		});
		final Calendar c = Calendar.getInstance();
		mYear = c.get(Calendar.YEAR);
		mMonth = c.get(Calendar.MONTH);
		mDay = c.get(Calendar.DAY_OF_MONTH);
		/*
		 * DefaultDate= mDay + "/" + mMonth + "/" + mYear;
		 * if(preference.getString(keySelf," ")==" "){
		 * dateEdittext1.setText(DefaultDate); }else{
		 * dateEdittext1.setText(preference.getString(keySelf," ")); }
		 */
	}

	@Override
	@Deprecated
	protected Dialog onCreateDialog(int id) {
		// TODO Auto-generated method stub
		switch (id) {
		case DATE_DIALOG_ID:
			return new DatePickerDialog(this, mDateSetListener, mYear, mMonth,
					mDay);
		}
		return null;
	}

	private void updateDisplay() {
		dateStr = mDay + "/" + mMonth + "/" + mYear;
		dateEdittext1.setText(dateStr);
	}

	private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {

		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			// TODO Auto-generated method stub
			mYear = year;
			mMonth = monthOfYear + 1;
			mDay = dayOfMonth;
			updateDisplay();
			Editor editor1 = preference.edit();
			editor1.putString(keySelf, dateEdittext1.getText().toString());
			editor1.commit();
		}
	};

	public void setDefaultDate() {
		Calendar cal = Calendar.getInstance();
		mYear = cal.get(Calendar.YEAR);
		mMonth = cal.get(Calendar.MONTH);
		mMonth = mMonth + 1;
		mDay = cal.get(Calendar.DAY_OF_MONTH);
		updateDisplay();
		Editor editor1 = preference.edit();
		editor1.putString(keySelf, dateEdittext1.getText().toString());
		editor1.commit();
	}
}
