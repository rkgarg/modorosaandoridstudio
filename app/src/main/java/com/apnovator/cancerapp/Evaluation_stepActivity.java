package com.apnovator.cancerapp;

import java.util.Locale;

import com.apnovator.cancerapp.R.string;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore.Images;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Gallery;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

public class Evaluation_stepActivity extends Activity {
	SharedPreferences preference;
	private String langSelection=" ";
	int message;
	//ScrollView myScroll;
	TextView text1;
	//ImageButton scrol_dwn;
	private Integer[] icons = { R.drawable.evaluation_icon1,
			R.drawable.evaluation_icon2, R.drawable.evaluation_icon3,
			R.drawable.evaluation_icon4, R.drawable.evaluation_icon5 };

	private Integer[] image = { R.drawable.icon_n, R.drawable.icon_nn,
			R.drawable.icon_girll_3, R.drawable.icon_gir_4,
			R.drawable.icon_5_girl };
	private Integer[] text = { R.string.evaluation1, R.string.evaluation2,
			R.string.evaluation3, R.string.evaluation4, R.string.evaluation5 };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Intent intent = getIntent();
		message = intent.getExtras().getInt("name");
		super.onCreate(savedInstanceState);
		preference  = getSharedPreferences("MyPREFERENCES",Context.MODE_WORLD_READABLE);
		langSelection=preference.getString("langSelect"," ");
		if(langSelection.compareTo("spanish")==0){
		Locale locale2 = new Locale("sp"); 
        Locale.setDefault(locale2);
			Configuration config = new Configuration();
			config.locale = locale2;
			getBaseContext().getResources().updateConfiguration(config,
			getBaseContext().getResources().getDisplayMetrics());
		}
		else if(langSelection.compareTo("english")==0){
			Locale locale2 = new Locale("en"); 
	        Locale.setDefault(locale2);
				Configuration config = new Configuration();
				config.locale = locale2;
				getBaseContext().getResources().updateConfiguration(config,
				getBaseContext().getResources().getDisplayMetrics());
		}
		else if(langSelection.compareTo("french")==0){
			Locale locale2 = new Locale("fr"); 
	        Locale.setDefault(locale2);
				Configuration config = new Configuration();
				config.locale = locale2;
				getBaseContext().getResources().updateConfiguration(config,
				getBaseContext().getResources().getDisplayMetrics());
		}
		else if(langSelection.compareTo("purtuge")==0){
			Locale locale2 = new Locale("pt");
			Locale.setDefault(locale2);
			Configuration config = new Configuration();
			config.locale = locale2;
			getBaseContext().getResources().updateConfiguration(config,
					getBaseContext().getResources().getDisplayMetrics());
		}
		setContentView(R.layout.activity_evaluation_step);
		ImageButton share_icon = (ImageButton) findViewById(R.id.share_icon);
	    // myScroll=(ScrollView)findViewById(R.id.myScroll);
		final ImageButton left_btn = (ImageButton) findViewById(R.id.left_btn);
		final ImageButton right_btn = (ImageButton) findViewById(R.id.right_btn);
		final Gallery gallery1 = (Gallery) findViewById(R.id.gallery);
		gallery1.setAdapter(new ImageAdapter(this));
		gallery1.setSelection(message);
		gallery1.setSpacing(0);

		//scrol_dwn=(ImageButton)findViewById(R.id.scrol_dwn);
		// gallery1.setSelection(message);
		
		left_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				right_btn.setEnabled(true);
				if (message == 0) {
					left_btn.setEnabled(false);
					
					// left_btn.setVisibility(View.INVISIBLE);
				} else {
					left_btn.setEnabled(true);
					message = message - 1;
					gallery1.setSelection(message);
				}
			}
		});
		right_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				left_btn.setEnabled(true);
				if (message == 4) {
					 right_btn.setEnabled(false);
					// right_btn.setVisibility(View.INVISIBLE);

				} else {
					right_btn.setEnabled(true);
					message = message + 1;
					gallery1.setSelection(message);
				}
			}
		});
		share_icon.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Uri imageUri = Uri.parse("android.resource://"
						+ getPackageName() + "/" + image[message]);
				Intent shareintent = new Intent(Intent.ACTION_SEND);
				shareintent.setType("image/png");
				shareintent.putExtra(Intent.EXTRA_STREAM, imageUri);
				shareintent.putExtra(Intent.EXTRA_TEXT,"http://bit.ly/ModoRosaAndroid");
			
				startActivity(Intent.createChooser(shareintent, "Share Image"));
				
			}
		});

	
	}
	public class ImageAdapter extends BaseAdapter {
		private Context mContext;

		public ImageAdapter(Context c) {
			// TODO Auto-generated constructor stub
			mContext = c;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return 5;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub

			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			LayoutInflater inflater = getLayoutInflater();
			final View row = inflater.inflate(R.layout.activity_evaluation_step2,
					parent, false);
			ImageView image1 = (ImageView) row.findViewById(R.id.imagee1);
			ImageView image2 = (ImageView) row.findViewById(R.id.imagee2);
			text1 = (TextView) row.findViewById(R.id.texte1);
			final ScrollView myScroll = (ScrollView)row.findViewById(R.id.sv2);
			ImageButton scrol_dwn=(ImageButton)row.findViewById(R.id.scrol_dwn);
			image1.setImageResource(icons[position]);
			image2.setImageResource(image[position]);
			text1.setText(text[position]);
			Typeface type2=Typeface.createFromAsset(getAssets(),"fonts/AvenirLTStd-Roman.otf");
			text1.setTypeface(type2);
			//text1.setMovementMethod(new ScrollingMovementMethod());
			scrol_dwn.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					//text1.setMovementMethod(new ScrollingMovementMethod());
					ScrollView sv=(ScrollView)row.findViewById(R.id.sv2);

					Log.v("check"," "+1);
					sv.scrollBy(0, 40);
				}
			});
			return (row);
		
		}
	}
		
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		//Intent in=new Intent(this,HomeMenu.class);
		//startActivity(in);
		//finish();
	}
	}
