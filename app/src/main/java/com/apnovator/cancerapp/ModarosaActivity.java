package com.apnovator.cancerapp;

import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;

public class ModarosaActivity extends Activity {
	String msg = " ";
	SharedPreferences preference;
	private String langSelection=" ";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		preference  = getSharedPreferences("MyPREFERENCES",Context.MODE_WORLD_READABLE);
		langSelection=preference.getString("langSelect"," ");
		if(langSelection.compareTo("spanish")==0){
		Locale locale2 = new Locale("sp"); 
        Locale.setDefault(locale2);
			Configuration config = new Configuration();
			config.locale = locale2;
			getBaseContext().getResources().updateConfiguration(config,
			getBaseContext().getResources().getDisplayMetrics());
		}
		else if(langSelection.compareTo("english")==0){
			Locale locale2 = new Locale("en"); 
	        Locale.setDefault(locale2);
				Configuration config = new Configuration();
				config.locale = locale2;
				getBaseContext().getResources().updateConfiguration(config,
				getBaseContext().getResources().getDisplayMetrics());
		}
		else if(langSelection.compareTo("french")==0){
			Locale locale2 = new Locale("fr"); 
	        Locale.setDefault(locale2);
				Configuration config = new Configuration();
				config.locale = locale2;
				getBaseContext().getResources().updateConfiguration(config,
				getBaseContext().getResources().getDisplayMetrics());
		}
		else if(langSelection.compareTo("purtuge")==0){
			Locale locale2 = new Locale("pt");
			Locale.setDefault(locale2);
			Configuration config = new Configuration();
			config.locale = locale2;
			getBaseContext().getResources().updateConfiguration(config,
					getBaseContext().getResources().getDisplayMetrics());
		}
		setContentView(R.layout.activity_modarosa);
		Button impacto = (Button) findViewById(R.id.impacto);
		Button id = (Button) findViewById(R.id.id);
		Button quienes_somos = (Button) findViewById(R.id.quienes_somos);
		Button button_como_puedo = (Button) findViewById(R.id.button_como_puedo);
		Button follow = (Button) findViewById(R.id.follow);
		ImageButton male_button = (ImageButton) findViewById(R.id.male_button);
		Typeface type2 = Typeface.createFromAsset(getAssets(),
				"fonts/AvenirLTStd-Roman.otf");
		quienes_somos.setTypeface(type2);
		impacto.setTypeface(type2);
		id.setTypeface(type2);
		button_como_puedo.setTypeface(type2);
		follow.setTypeface(type2);
		follow.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(ModarosaActivity.this,
						FollowUsActivity.class);
				startActivity(intent);
				
			}
			
		});
		impacto.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				msg = "link3";
				Intent intent = new Intent(ModarosaActivity.this,
						AboutUsPage1.class);
				intent.putExtra("name", msg);
				startActivity(intent);
			}
		});
		id.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				msg = "link4";
				Intent intent = new Intent(ModarosaActivity.this,
						AboutUsPage1.class);
				intent.putExtra("name", msg);
				startActivity(intent);
			}
		});
		male_button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// Intent emailIntent = new Intent(Intent.ACTION_SENDTO,
				// Uri.fromParts(
				// //"mailto", "abc@gmail.com", null));
				// emailIntent.putExtra(Intent.EXTRA_SUBJECT,
				// "This is my subject text");
				// startActivity(Intent.createChooser(emailIntent, null));
				String emailText=getResources().getString(R.string.emailTextModa);
				String emailSubject=getResources().getString(R.string.emailSubjectModa);
				Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri
						.fromParts("mailto","modorosa@tripartitacomunicaciones.com", null));
				// emailIntent.setType("text/plain");
				emailIntent.putExtra(Intent.EXTRA_SUBJECT,
						emailSubject);
				emailIntent.putExtra(Intent.EXTRA_TEXT,emailText);
				final PackageManager pm = getPackageManager();
				final List<ResolveInfo> matches = pm.queryIntentActivities(
						emailIntent, 0);
				ResolveInfo best = null;
				for (final ResolveInfo info : matches)
					if (info.activityInfo.packageName.endsWith(".gm")
							|| info.activityInfo.name.toLowerCase().contains(
									"gmail"))
						best = info;
				if (best != null)
					emailIntent.setClassName(best.activityInfo.packageName,
							best.activityInfo.name);
				startActivity(emailIntent);

			}
		});
		quienes_somos.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				msg = "link6";
				Intent intent = new Intent(ModarosaActivity.this,
						AboutUsPage1.class);
				intent.putExtra("name", msg);
				startActivity(intent);

			}
		});
		button_como_puedo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				msg = "link7";
				Intent intent = new Intent(ModarosaActivity.this,
						AboutUsPage1.class);
				intent.putExtra("name", msg);
				startActivity(intent);

			}
		});
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub

		 Intent in = new Intent(this, HomeMenu.class);
		startActivity(in);
		finish();
	
		
}
}