package com.apnovator.cancerapp;

import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

public class Splash2Activity extends Activity {
	SharedPreferences preference;
	private String langSelection=" ";
	private static int splash_time_out=5000;
	//private Integer[] mThumbIds={R.drawable.splash2_image1, R.drawable.splash2_image2, R.drawable.splash2_image3,
			//R.drawable.splash2_image4, R.drawable.splash2_image5, R.drawable.splash2_image6,
			//R.drawable.splash2_image7, R.drawable.splash2_image8, R.drawable.splash2_image9,
			//R.drawable.splash2_image10, R.drawable.splash2_image11, R.drawable.splash2_image12,
			//R.drawable.splash2_image13, R.drawable.splash2_image14
			//};
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		preference  = getSharedPreferences("MyPREFERENCES",Context.MODE_WORLD_READABLE);
		langSelection=preference.getString("langSelect"," ");
		if(langSelection.compareTo("spanish")==0){
		Locale locale2 = new Locale("sp"); 
        Locale.setDefault(locale2);
			Configuration config = new Configuration();
			config.locale = locale2;
			getBaseContext().getResources().updateConfiguration(config,
			getBaseContext().getResources().getDisplayMetrics());
		}
		else if(langSelection.compareTo("english")==0){
			Locale locale2 = new Locale("en"); 
	        Locale.setDefault(locale2);
				Configuration config = new Configuration();
				config.locale = locale2;
				getBaseContext().getResources().updateConfiguration(config,
				getBaseContext().getResources().getDisplayMetrics());
		}
		else if(langSelection.compareTo("french")==0){
			Locale locale2 = new Locale("fr"); 
	        Locale.setDefault(locale2);
				Configuration config = new Configuration();
				config.locale = locale2;
				getBaseContext().getResources().updateConfiguration(config,
				getBaseContext().getResources().getDisplayMetrics());
		}
		else if(langSelection.compareTo("purtuge")==0){
			Locale locale2 = new Locale("pt");
			Locale.setDefault(locale2);
			Configuration config = new Configuration();
			config.locale = locale2;
			getBaseContext().getResources().updateConfiguration(config,
					getBaseContext().getResources().getDisplayMetrics());
		}
		setContentView(R.layout.activity_splash2);
		Editor ed = preference.edit();
		ed.putBoolean("activity_executed1", true);
		ed.commit();
	
		//GridView gridview = (GridView) findViewById(R.id.gridview);
		//gridview.setAdapter(new ImageAdapter(this));
		new Handler().postDelayed(new Runnable(){

			@Override
			public void run() {
				// TODO Auto-generated method stub
				Intent i=new Intent(Splash2Activity.this,HomeMenu.class);
				startActivity(i);
				finish();
			}
	
	},splash_time_out );
}
	//public class ImageAdapter extends BaseAdapter{
		//private Context mcontext; 
		//public ImageAdapter(Context c){
			//mcontext = c;
		//}
		//@Override
		//public int getCount() {
			// TODO Auto-generated method stub
			//return mThumbIds.length;
		//}
		//@Override
		//public Object getItem(int position) {
			// TODO Auto-generated method stub
			//return null;
		//}
		//@Override
		//public long getItemId(int position) {
			// TODO Auto-generated method stub
			//return 0;
		//}
		//@Override
			// TODO Auto-generated method stub
			//ImageView imageView = new ImageView(mcontext);
			//imageView.setLayoutParams(new GridView.LayoutParams(85, 85));
			//imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
			//imageView.setPadding(8, 8, 8, 8);
			//imageView.setImageResource(mThumbIds[position]);
			//return imageView;
			
		//}
	}

