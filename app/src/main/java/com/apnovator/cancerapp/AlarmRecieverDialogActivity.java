package com.apnovator.cancerapp;

import java.util.List;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;

public class AlarmRecieverDialogActivity extends Activity {
	MediaPlayer mediaPlayer;
	int alarm1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// setContentView(R.layout.activity_alarm_reciever_dialog);
		Intent intent = getIntent();
		alarm1 = intent.getIntExtra("alarm1", 0);
		Log.v("check alarm", " " + alarm1);
		SharedPreferences preference = getSharedPreferences("MyPREFERENCES",
				Context.MODE_PRIVATE);
		int message1 = preference.getInt("sound1", 0);
		int message2 = preference.getInt("sound2", 0);
		int message3 = preference.getInt("sound3", 0);
		int message4 = preference.getInt("sound4", 0);
		String notificationText = " ";
		if (alarm1 == 1) {
			notificationText = getResources().getString(
					R.string.NotificationtextSelf);
			if (message1 == 1) {
				mediaPlayer = MediaPlayer.create(
						AlarmRecieverDialogActivity.this, R.raw.sound1);
				mediaPlayer.start();
			} else if (message1 == 2) {
				mediaPlayer = MediaPlayer.create(
						AlarmRecieverDialogActivity.this, R.raw.sound2);
				mediaPlayer.start();
			} else if (message1 == 3) {
				mediaPlayer = MediaPlayer.create(
						AlarmRecieverDialogActivity.this, R.raw.sound3);
				mediaPlayer.start();
			} else if (message1 == 6) {
				// myNotification.sound=Uri.parse("android.resource://com.apnovator.cancerapp/raw/sound4");
				Vibrator vibrator = (Vibrator) AlarmRecieverDialogActivity.this
						.getSystemService(Context.VIBRATOR_SERVICE);
				vibrator.vibrate(2000);

			}
		} else if (alarm1 == 2) {
			notificationText = getResources().getString(
					R.string.NotificationtextGyna);

			if (message2 == 1) {
				mediaPlayer = MediaPlayer.create(
						AlarmRecieverDialogActivity.this, R.raw.sound1);
				mediaPlayer.start();
			} else if (message2 == 2) {
				mediaPlayer = MediaPlayer.create(
						AlarmRecieverDialogActivity.this, R.raw.sound2);
				mediaPlayer.start();
			} else if (message2 == 3) {
				mediaPlayer = MediaPlayer.create(
						AlarmRecieverDialogActivity.this, R.raw.sound3);
				mediaPlayer.start();
			} else if (message2 == 6) {
				// myNotification.sound=Uri.parse("android.resource://com.apnovator.cancerapp/raw/sound4");
				Vibrator vibrator = (Vibrator) AlarmRecieverDialogActivity.this
						.getSystemService(Context.VIBRATOR_SERVICE);
				vibrator.vibrate(2000);

			}
		} else if (alarm1 == 3) {
			notificationText = getResources().getString(
					R.string.NotificationtextMamo);

			if (message3 == 1) {
				mediaPlayer = MediaPlayer.create(
						AlarmRecieverDialogActivity.this, R.raw.sound1);
				mediaPlayer.start();
			} else if (message3 == 2) {
				mediaPlayer = MediaPlayer.create(
						AlarmRecieverDialogActivity.this, R.raw.sound2);
				mediaPlayer.start();
			} else if (message3 == 3) {
				mediaPlayer = MediaPlayer.create(
						AlarmRecieverDialogActivity.this, R.raw.sound3);
				mediaPlayer.start();
			} else if (message3 == 6) {
				// myNotification.sound=Uri.parse("android.resource://com.apnovator.cancerapp/raw/sound4");
				Vibrator vibrator = (Vibrator) AlarmRecieverDialogActivity.this
						.getSystemService(Context.VIBRATOR_SERVICE);
				vibrator.vibrate(2000);

			}
		} else if (alarm1 == 4) {
			notificationText = getResources().getString(
					R.string.NotificationtextEco);

			if (message4 == 1) {
				mediaPlayer = MediaPlayer.create(
						AlarmRecieverDialogActivity.this, R.raw.sound1);
				mediaPlayer.start();
			} else if (message4 == 2) {
				mediaPlayer = MediaPlayer.create(
						AlarmRecieverDialogActivity.this, R.raw.sound2);
				mediaPlayer.start();
			} else if (message4 == 3) {
				mediaPlayer = MediaPlayer.create(
						AlarmRecieverDialogActivity.this, R.raw.sound3);
				mediaPlayer.start();
			} else if (message4 == 6) {
				// myNotification.sound=Uri.parse("android.resource://com.apnovator.cancerapp/raw/sound4");
				Vibrator vibrator = (Vibrator) AlarmRecieverDialogActivity.this
						.getSystemService(Context.VIBRATOR_SERVICE);
				vibrator.vibrate(2000);

			}
		}
		showAlertDialog(notificationText);
	}

	public void showAlertDialog(String message) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		CharSequence title = null;
		if (alarm1 == 1) {
			title = AlarmRecieverDialogActivity.this.getResources().getString(
					R.string.SelfTitle);
		} else if (alarm1 == 2) {
			title = AlarmRecieverDialogActivity.this.getResources().getString(
					R.string.GynaTitle);
		} else if (alarm1 == 3) {
			title = AlarmRecieverDialogActivity.this.getResources().getString(
					R.string.MamoTitle);
		} else if (alarm1 == 4) {
			title = AlarmRecieverDialogActivity.this.getResources().getString(
					R.string.EcoTitle);
		}
		builder.setTitle(title);
		builder.setMessage(message);
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface arg0, int arg1) {
				// do something when the OK button is clicked
				if (mediaPlayer != null && mediaPlayer.isPlaying()) {
					mediaPlayer.stop();
					// mMediaPlayer.release();
				}
				AlarmRecieverDialogActivity.this.finish();
				// UnityPlayer.UnitySendMessage("Main Camera",
				// "OnPushNotification", "");
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
		TextView messageText = (TextView) alert
			    .findViewById(android.R.id.message);
			  messageText.setGravity(Gravity.CENTER);
			 int textViewId = alert.getContext().getResources()
					    .getIdentifier("android:id/alertTitle", null, null);
					  TextView tv = (TextView) alert.findViewById(textViewId);
					  if (tv != null) {
					   tv.setTextColor(getResources().getColor(
					     android.R.color.white));
					   tv.setGravity(Gravity.CENTER);
	}
	}
}
