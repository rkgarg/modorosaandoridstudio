package com.apnovator.cancerapp;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.TextView;

public class HowModaRosa extends Activity {
	// int scrollAmount;
	TextView how_text;
	ScrollView sv;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_how_moda_rosa);
		how_text = (TextView) findViewById(R.id.how_text);
		ImageButton howmoda_btn = (ImageButton) findViewById(R.id.howmoda_btn);
		sv = (ScrollView) findViewById(R.id.sv);
		Typeface type2=Typeface.createFromAsset(getAssets(),"fonts/AvenirLTStd-Roman.otf");
		how_text.setTypeface(type2);
		howmoda_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// how_text.setMovementMethod(new ScrollingMovementMethod());

				sv.scrollBy(0, 60);

			}
		});
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		Intent in = new Intent(this, ModarosaActivity.class);
		startActivity(in);
		finish();
	}
}
