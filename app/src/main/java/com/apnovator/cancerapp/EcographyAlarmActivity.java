package com.apnovator.cancerapp;

import java.util.Calendar;
import java.util.Locale;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

public class EcographyAlarmActivity extends Activity {
	private int aYear;
	private int aMonth;
	private int aDay;
	static final int DATE_DIALOG_ID = 0;
	static final int TIME_DIALOG_ID = 1;
	private int aHour;
	private int aMinute;
	private String setDate = " ";

	MamographyAlertReceiver alarm2;
	int confirmEco;
	public static final String keyEco = "keyEco";
	TextView ecoAlert1;
	SharedPreferences preference;
	String DefaultDate;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// public static final String key1 = "key1";
		String langSelection = " ";
		preference = getSharedPreferences("MyPREFERENCES",
				Context.MODE_WORLD_READABLE);
		langSelection = preference.getString("langSelect", " ");
		if (langSelection.compareTo("spanish") == 0) {
			Locale locale2 = new Locale("sp");
			Locale.setDefault(locale2);
			Configuration config = new Configuration();
			config.locale = locale2;
			getBaseContext().getResources().updateConfiguration(config,
					getBaseContext().getResources().getDisplayMetrics());
		} else if (langSelection.compareTo("english") == 0) {
			Locale locale2 = new Locale("en");
			Locale.setDefault(locale2);
			Configuration config = new Configuration();
			config.locale = locale2;
			getBaseContext().getResources().updateConfiguration(config,
					getBaseContext().getResources().getDisplayMetrics());
		}
		else if(langSelection.compareTo("french")==0){
			Locale locale2 = new Locale("fr"); 
	        Locale.setDefault(locale2);
				Configuration config = new Configuration();
				config.locale = locale2;
				getBaseContext().getResources().updateConfiguration(config,
				getBaseContext().getResources().getDisplayMetrics());
		}
		else if(langSelection.compareTo("purtuge")==0){
			Locale locale2 = new Locale("pt");
			Locale.setDefault(locale2);
			Configuration config = new Configuration();
			config.locale = locale2;
			getBaseContext().getResources().updateConfiguration(config,
					getBaseContext().getResources().getDisplayMetrics());
		}
		setContentView(R.layout.activity_ecography_alarm);

		ecoAlert1 = (TextView) findViewById(R.id.ecoAlert1);
		TextView ecoTextView1 = (TextView) findViewById(R.id.ecoTextView1);
		Typeface type2 = Typeface.createFromAsset(getAssets(),
				"fonts/AvenirLTStd-Roman.otf");
		ecoAlert1.setTypeface(type2);
		ecoTextView1.setTypeface(type2);
		if (preference.getString(keyEco, " ").compareTo(" ") == 0) {
			setDefaultDate();
		} else {
			ecoAlert1.setText(preference.getString(keyEco, " "));
		}
		Button bck_btn1 = (Button) findViewById(R.id.bck_btnEco);
		bck_btn1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// onBackPressed();
				confirmEco = 1;

				Intent returnIntent = new Intent();

				returnIntent.putExtra("ecoDateString", ecoAlert1.getText()
						.toString());

				returnIntent.putExtra("confirmEco", confirmEco);

				setResult(RESULT_OK, returnIntent);
				finish();
			}
		});
		ecoAlert1.setOnClickListener(new OnClickListener() {

			@SuppressWarnings("deprecation")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showDialog(DATE_DIALOG_ID);

			}
		});
		Calendar c = Calendar.getInstance();
		aYear = c.get(Calendar.YEAR);
		aMonth = c.get(Calendar.MONTH);
		aDay = c.get(Calendar.DAY_OF_MONTH);
		/*
		 * DefaultDate= aDay + "/" + aMonth + "/" + aYear; if
		 * (preference.getString(keyEco, " ")==" ") {
		 * ecoAlert1.setText(DefaultDate); Log.v("default",DefaultDate); } else{
		 * Log.v("default",preference.getString(keyEco, " "));
		 * ecoAlert1.setText(preference.getString(keyEco, " ")); }
		 */
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
	}

	@Override
	@Deprecated
	protected Dialog onCreateDialog(int id) {
		// TODO Auto-generated method stub
		switch (id) {
		case DATE_DIALOG_ID:
			return new DatePickerDialog(this, aDateSetListener, aYear, aMonth,
					aDay);
		}
		return null;
	}

	
	  public void mamographyAlert(View v) { confirmEco=1;
	 
	 Intent returnIntent = new Intent();
	 
	  returnIntent.putExtra("ecoDateString",ecoAlert1.getText().toString());
	  
	 returnIntent.putExtra("confirmEco", confirmEco);
	 
	  setResult(RESULT_OK, returnIntent); finish(); }
	 
	private void updateDisplay() {
		setDate = aDay + "/" + aMonth + "/" + aYear;
		ecoAlert1.setText(setDate);

	}

	private DatePickerDialog.OnDateSetListener aDateSetListener = new DatePickerDialog.OnDateSetListener() {

		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			// TODO Auto-generated method stub
			aYear = year;
			aMonth = monthOfYear + 1;
			aDay = dayOfMonth;
			updateDisplay();
			Editor editor1 = preference.edit();
			editor1.putString(keyEco, setDate);
			editor1.commit();

		}
	};

	public void setDefaultDate() {
		Calendar cal = Calendar.getInstance();
		aYear = cal.get(Calendar.YEAR);
		aMonth = cal.get(Calendar.MONTH);
		aMonth = aMonth + 1;
		aDay = cal.get(Calendar.DAY_OF_MONTH);
		updateDisplay();
		Editor editor1 = preference.edit();
		editor1.putString(keyEco, setDate);
		editor1.commit();
	}
}
