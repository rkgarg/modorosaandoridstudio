package com.apnovator.cancerapp;

import java.util.Calendar;
import java.util.Locale;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.TimePicker;

public class SetTimeActivity extends Activity {
	static final int TIME_DIALOG_ID = 1;
	private int mHourSelf;
	private int mMinuteSelf;
	private int mHourGyna;
	private int mMinuteGyna;
	private int mHourMamo;
	private int mMinuteMamo;
	private int mHourEco;
	private int mMinuteEco;
	private int alarmId;
	private int nHour;
	private int nMinute;
	SharedPreferences preference;
	private static final String confirmTimeSelf = "confirmTimeSelf";
	private static final String confirmTimeGyna = "confirmTimeGyna";
	private static final String confirmTimeMamo = "confirmTimeMamo";
	private static final String confirmTimeEco = "confirmTimeEco";
	private static final String nHourSelf = "nHourSelf";
	private static final String nMinuteSelf = "nMinuteSelf";
	private static final String nHourGyna = "nHourGyna";
	private static final String nMinuteGyna = "nMinuteGyna";
	private static final String nHourMamo = "nHourMamo";
	private static final String nMinuteMamo = "nMinuteMamo";
	 String nHourEco="nHourEco";
	 String nMinuteEco="nMinuteEco";
	private String setTimeString = " ";
	TextView setTime;
	private String langSelection=" ";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		preference = getSharedPreferences("MyPREFERENCES",
				Context.MODE_WORLD_READABLE);
		langSelection=preference.getString("langSelect"," ");
		if(langSelection.compareTo("spanish")==0){
		Locale locale2 = new Locale("sp"); 
        Locale.setDefault(locale2);
			Configuration config = new Configuration();
			config.locale = locale2;
			getBaseContext().getResources().updateConfiguration(config,
			getBaseContext().getResources().getDisplayMetrics());
		}
		else if(langSelection.compareTo("english")==0){
			Locale locale2 = new Locale("en"); 
	        Locale.setDefault(locale2);
				Configuration config = new Configuration();
				config.locale = locale2;
				getBaseContext().getResources().updateConfiguration(config,
				getBaseContext().getResources().getDisplayMetrics());
		}
		else if(langSelection.compareTo("french")==0){
			Locale locale2 = new Locale("fr"); 
	        Locale.setDefault(locale2);
				Configuration config = new Configuration();
				config.locale = locale2;
				getBaseContext().getResources().updateConfiguration(config,
				getBaseContext().getResources().getDisplayMetrics());
		}
		else if(langSelection.compareTo("purtuge")==0){
			Locale locale2 = new Locale("pt");
			Locale.setDefault(locale2);
			Configuration config = new Configuration();
			config.locale = locale2;
			getBaseContext().getResources().updateConfiguration(config,
					getBaseContext().getResources().getDisplayMetrics());
		}
		setContentView(R.layout.activity_set_time);
		Intent intent = getIntent();
		alarmId = intent.getIntExtra("AlarmId", 0);
		
		
		setTime = (TextView) findViewById(R.id.setTime);
		Typeface type2=Typeface.createFromAsset(getAssets(),"fonts/AvenirLTStd-Roman.otf");
		TextView setTimeText=(TextView)findViewById(R.id.setTimeText);
		setTimeText.setTypeface(type2);
		setTime.setTypeface(type2);
		Button bck_btnTime=(Button)findViewById(R.id.bck_btnTime);
		bck_btnTime.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//onBackPressed();
				Intent returnIntent = new Intent();
				if (alarmId == 1) {
					int timeSelf = 1;

					returnIntent.putExtra("timeSelf", timeSelf);
				} else if (alarmId == 2) {

					int timeGyna = 1;
					returnIntent.putExtra("timeGyna", timeGyna);
				} else if (alarmId == 3) {
					int timeMamo = 1;
					returnIntent.putExtra("timeMamo", timeMamo);
				}
				else if (alarmId == 4) {
					int timeEco = 1;
					returnIntent.putExtra("timeEco", timeEco);
				}
				// returnIntent.putExtra("nd", mDay);
				setResult(RESULT_OK, returnIntent);
				finish();
			}
		});
		if (alarmId == 1) {
			setTime.setText(preference.getString(confirmTimeSelf, "08:00"));
		} else if (alarmId == 2) {
			setTime.setText(preference.getString(confirmTimeGyna, "08:00"));
		} else if (alarmId == 3) {
			setTime.setText(preference.getString(confirmTimeMamo, "08:00"));
		}
		else if (alarmId == 4) {
			setTime.setText(preference.getString(confirmTimeEco, "08:00"));
		}
		ImageButton timeSave = (ImageButton) findViewById(R.id.timeSave);
		setTime.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showDialog(TIME_DIALOG_ID);
				
			}
		});
		final Calendar c = Calendar.getInstance();
		nHour = c.get(Calendar.HOUR_OF_DAY);
		nMinute = c.get(Calendar.MINUTE);
		timeSave.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent returnIntent = new Intent();
				if (alarmId == 1) {
					int timeSelf = 1;

					returnIntent.putExtra("timeSelf", timeSelf);
				} else if (alarmId == 2) {

					int timeGyna = 1;
					returnIntent.putExtra("timeGyna", timeGyna);
				} else if (alarmId == 3) {
					int timeMamo = 1;
					returnIntent.putExtra("timeMamo", timeMamo);
				}
				else if (alarmId == 4) {
					int timeEco = 1;
					returnIntent.putExtra("timeEco", timeEco);
				}
				// returnIntent.putExtra("nd", mDay);
				setResult(RESULT_OK, returnIntent);
				finish();
			}
		});
	}

	protected Dialog onCreateDialog(int id) {
		// TODO Auto-generated method stub
		switch (id) {
		case TIME_DIALOG_ID:
			return new TimePickerDialog(this, nTimeSetListener, nHour, nMinute,
					false);
		}
		return null;
	}

	private void updateDisplay() {
		if (alarmId == 1) {
			if(mMinuteSelf<10){
				setTimeString = " " + mHourSelf + ":" + "0"+mMinuteSelf;
			}else{
			setTimeString = " " + mHourSelf + ":" + mMinuteSelf;
			}
			setTime.setText(setTimeString);
		} else if (alarmId == 2) {
			if(mMinuteGyna<10){
				setTimeString = " " + mHourGyna + ":" + "0"+mMinuteGyna;
			}else{
			setTimeString = " " + mHourGyna + ":" + mMinuteGyna;
			}
			setTime.setText(setTimeString);
		} else if (alarmId == 3) {
			if(mMinuteMamo<10){
				setTimeString = " " + mHourMamo + ":" + "0"+mMinuteMamo;
			}else{
			setTimeString = " " + mHourMamo + ":" + mMinuteMamo;
			}
			setTime.setText(setTimeString);
		}
		 else if (alarmId == 4) {
				if(mMinuteEco<10){
					setTimeString = " " + mHourEco + ":" + "0"+mMinuteEco;
				}else{
				setTimeString = " " + mHourEco + ":" + mMinuteEco;
				}
				setTime.setText(setTimeString);
			}

	}

	private TimePickerDialog.OnTimeSetListener nTimeSetListener = new TimePickerDialog.OnTimeSetListener() {

		@Override
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			// TODO Auto-generated method stub
			Editor editor1 = preference.edit();
			if (alarmId == 1) {
				mHourSelf = hourOfDay;
				mMinuteSelf = minute;
				updateDisplay();
				editor1.putString(confirmTimeSelf, setTime.getText().toString());
				editor1.putInt("nHourSelf", mHourSelf);
				editor1.putInt("nMinuteSelf", mMinuteSelf);
			} else if (alarmId == 2) {
				mHourGyna = hourOfDay;
				mMinuteGyna = minute;
				updateDisplay();
				editor1.putString(confirmTimeGyna, setTime.getText().toString());
				editor1.putInt("nHourGyna", mHourGyna);
				editor1.putInt("nMinuteGyna", mMinuteGyna);
			} else if (alarmId == 3) {
				mHourMamo = hourOfDay;
				mMinuteMamo = minute;
				updateDisplay();
				editor1.putString(confirmTimeMamo, setTime.getText().toString());
				editor1.putInt("nHourMamo", mHourMamo);
				editor1.putInt("nMinuteMamo", mMinuteMamo);
			}
			 else if (alarmId == 4) {
				
					mHourEco = hourOfDay;
					mMinuteEco = minute;
					updateDisplay();
					editor1.putString(confirmTimeEco, setTime.getText().toString());
					editor1.putInt("nHourEco", mHourEco);
					editor1.putInt("nMinuteEco", mMinuteEco);
				}
        editor1.commit();
		}
	};
}
