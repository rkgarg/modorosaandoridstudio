package com.apnovator.cancerapp;

import java.io.IOException;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayer.ErrorReason;
import com.google.android.youtube.player.YouTubePlayer.OnInitializedListener;
import com.google.android.youtube.player.YouTubePlayer.PlaybackEventListener;
import com.google.android.youtube.player.YouTubePlayer.PlayerStateChangeListener;
import com.google.android.youtube.player.YouTubePlayer.Provider;
import com.google.android.youtube.player.YouTubePlayerFragment;
import com.google.android.youtube.player.YouTubePlayerView;

import android.app.Activity;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

public class Video extends YouTubeBaseActivity  {
	private MediaPlayer mMediaPlayer;
	public static final String API_KEY = "AIzaSyDy6GR8aYdJD4_57DIuKhpliubeOUOxV1Y";
	public static final String VIDEO_ID = "OJ98XQMX_3w";
	public static final String VIDEO_ID1 = "v= qjB_0IUdqjA";
	ScrollView videoScroll;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_video);
		YouTubePlayerView youTubePlayerView = (YouTubePlayerView) findViewById(R.id.youtube_player);
		YouTubePlayerView youTubePlayerView1 = (YouTubePlayerView) findViewById(R.id.youtube_player1);
		//youtube_player=(YouTubePlayerFragment)getFragmentManager()
		  // .findFragmentById(R.id.youtube_player);
		//youtube_player1=(YouTubePlayerFragment)getFragmentManager()
				 //  .findFragmentById(R.id.youtube_player1);
		youTubePlayerView.initialize(API_KEY, new YouTubePlayer.OnInitializedListener() {
			
			
			@Override
			public void onInitializationSuccess(Provider arg0, YouTubePlayer arg1,
					boolean arg2) {
				// TODO Auto-generated method stub
				arg1.setPlayerStateChangeListener(playerStateChangeListener1);
				arg1.setPlaybackEventListener(playbackEventListener1);
				
				if (!arg2) {
					arg1.cueVideo(VIDEO_ID);
					arg1.setShowFullscreenButton(arg2);
				}
			
			}
			
			@Override
			public void onInitializationFailure(Provider arg0,
					YouTubeInitializationResult arg1) {
				// TODO Auto-generated method stub
				loadError();
				
			}

			
		});
		youTubePlayerView1.initialize(API_KEY,new YouTubePlayer.OnInitializedListener() {
			
			@Override
			public void onInitializationSuccess(Provider arg0, YouTubePlayer arg1,
					boolean arg2) {
				
				// TODO Auto-generated method stub
				arg1.setPlayerStateChangeListener(playerStateChangeListener2);
				arg1.setPlaybackEventListener(playbackEventListener2);
				
				if (!arg2) {
					arg1.cueVideo(VIDEO_ID);
					arg1.setShowFullscreenButton(arg2);
				}
			}
			
			@Override
			public void onInitializationFailure(Provider arg0,
					YouTubeInitializationResult arg1) {
				// TODO Auto-generated method stub
				loadError();

			}
		});
		
		ImageButton btn_Sound = (ImageButton) findViewById(R.id.btn_Sound1);
		Typeface type1=Typeface.createFromAsset(getAssets(),"fonts/AvenirLTStd-Heavy.otf");
		Typeface type2=Typeface.createFromAsset(getAssets(),"fonts/AvenirLTStd-Roman.otf");
		TextView videoText2 = (TextView) findViewById(R.id.videoText2);
		TextView videoText1 = (TextView) findViewById(R.id.videoText1);
		videoScroll=(ScrollView)findViewById(R.id.videoScroll);
		videoText1.setTypeface(type1);
		videoText2.setTypeface(type2);
		TextView videoText4 = (TextView) findViewById(R.id.videoText4);
		TextView videoText3= (TextView) findViewById(R.id.videoText3);
		videoText3.setTypeface(type1);
		videoText4.setTypeface(type2);
		ImageButton downVideo=(ImageButton)findViewById(R.id.downVideo);
		downVideo.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				videoScroll.scrollBy(0,40);
			}
		});
		//text.setTextSize(getResources().getDimension(R.dimen.text_size));
		mMediaPlayer = MediaPlayer.create(Video.this, R.raw.sound1);
		btn_Sound.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if (mMediaPlayer != null && mMediaPlayer.isPlaying()) {
					mMediaPlayer.pause();
					// mMediaPlayer.release();
				} else {
					try {
						mMediaPlayer.prepare();
					} catch (IllegalStateException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					mMediaPlayer.start();
				}

			}
		});
	}
	private void loadError() {
		// TODO Auto-generated method stub
		Toast.makeText(this, "Failured to Initialize!", Toast.LENGTH_LONG)
		.show();
	}
	private PlaybackEventListener playbackEventListener2 = new PlaybackEventListener() {

		@Override
		public void onBuffering(boolean arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onPaused() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onPlaying() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onSeekTo(int arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onStopped() {
			// TODO Auto-generated method stub
			
		}
		
	};
	private PlayerStateChangeListener playerStateChangeListener2 = new PlayerStateChangeListener() {

		@Override
		public void onAdStarted() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onError(ErrorReason arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onLoaded(String arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onLoading() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onVideoEnded() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onVideoStarted() {
			// TODO Auto-generated method stub
			
		}
		
	};
	private PlaybackEventListener playbackEventListener1 = new PlaybackEventListener() {

			@Override
			public void onBuffering(boolean arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onPaused() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onPlaying() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSeekTo(int arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onStopped() {
				// TODO Auto-generated method stub
				
			}
			
		};
		
		private PlayerStateChangeListener playerStateChangeListener1 = new PlayerStateChangeListener() {

			@Override
			public void onAdStarted() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onError(ErrorReason arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onLoaded(String arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onLoading() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onVideoEnded() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onVideoStarted() {
				// TODO Auto-generated method stub
				
			}
			
		};
	

	

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		if (mMediaPlayer != null && mMediaPlayer.isPlaying()) {
			mMediaPlayer.stop();
			mMediaPlayer.release();
		}
	}
}
