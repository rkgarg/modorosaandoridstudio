package com.apnovator.cancerapp;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.net.ParseException;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract.Data;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

public class SelfExamActivity extends Activity {
	private String selfString = " ";
	private int cycleLength;
	private int n2;
	private int sound;
	private String mamoDateString = " ";
	private String ecoDateString = " ";
	private String appoinment = " ";
	long duration;
	long apointmentDuration;
	long mamoDuration;
	long ecoDuration;
	private int alarmIds;
	private String setDate = " ";
	private SelfExamAlarmReceive alarm;
	Uri path;
	private int mHourSelf;
	private int mMinuteSelf;
	private int mHourGyna;
	private int mMinuteGyna;
	private int mHourEco;
	private int mMinuteEco;
	private int mHourMamo;
	private int mMinuteMamo;
	private static final String confirmSelfAlarm = "confirmSelfAlarm";
	private static final String confirmGynaAlarm = "confirmGynaAlarm";
	private static final String confirmMamoAlarm = "confirmMamoAlarm";
	private static final String confirmEcoAlarm = "confirmEcoAlarm";
	SharedPreferences preference;
	String confirmGynaAlarm1 = " ";
	String confirmSelfAlarm1 = " ";
	String confirmMamoAlarm1 = " ";
	String confirmEcoAlarm1 = " ";
	private String langSelection = " ";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		preference = getSharedPreferences("MyPREFERENCES",
				Context.MODE_WORLD_READABLE);
		langSelection = preference.getString("langSelect", " ");
		if (langSelection.compareTo("spanish") == 0) {
			Locale locale2 = new Locale("sp");
			Locale.setDefault(locale2);
			Configuration config = new Configuration();
			config.locale = locale2;
			getBaseContext().getResources().updateConfiguration(config,
					getBaseContext().getResources().getDisplayMetrics());
		} else if (langSelection.compareTo("english") == 0) {
			Locale locale2 = new Locale("en");
			Locale.setDefault(locale2);
			Configuration config = new Configuration();
			config.locale = locale2;
			getBaseContext().getResources().updateConfiguration(config,
					getBaseContext().getResources().getDisplayMetrics());
		}
		else if(langSelection.compareTo("french")==0){
			Locale locale2 = new Locale("fr"); 
	        Locale.setDefault(locale2);
				Configuration config = new Configuration();
				config.locale = locale2;
				getBaseContext().getResources().updateConfiguration(config,
				getBaseContext().getResources().getDisplayMetrics());
		}
		else if(langSelection.compareTo("purtuge")==0){
			Locale locale2 = new Locale("pt");
			Locale.setDefault(locale2);
			Configuration config = new Configuration();
			config.locale = locale2;
			getBaseContext().getResources().updateConfiguration(config,
					getBaseContext().getResources().getDisplayMetrics());
		}
		setContentView(R.layout.activity_self_exam);
		Intent intent = getIntent();
		alarmIds = intent.getIntExtra("alarmId", 0);

		Button getPeriodDate = (Button) findViewById(R.id.getPeriodDate);
		Button getDate = (Button) findViewById(R.id.getDate);
		Button getTime = (Button) findViewById(R.id.getAlertTime);
		Button save = (Button) findViewById(R.id.Save);
		Typeface type2 = Typeface.createFromAsset(getAssets(),
				"fonts/AvenirLTStd-Roman.otf");
		Button getAlarmTone = (Button) findViewById(R.id.getAlertType);
		ImageButton icon = (ImageButton) findViewById(R.id.icon);
		getPeriodDate.setTypeface(type2);
		getDate.setTypeface(type2);
		getTime.setTypeface(type2);
		getAlarmTone.setTypeface(type2);
		save.setTypeface(type2);

		// Typeface.createFromAsset(getAssets(),"fonts/AvenirLTStd-Heavy_0.otf");
		Button getAlarmDate = (Button) findViewById(R.id.getAlarmDate);
		getAlarmDate.setTypeface(type2);
		getAlarmTone.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(SelfExamActivity.this,
						SoundActivity.class);
				intent.putExtra("AlarmId", alarmIds);
				startActivityForResult(intent, 2);
			}
		});
		// n3 = n3 + 8;
		if (alarmIds == 1) {
			icon.setImageResource(R.drawable.alerta_para_autoexamen);
			getAlarmDate.setVisibility(View.GONE);
		} else if (alarmIds == 2) {
			icon.setImageResource(R.drawable.alerta_para_cita_conel_button);
			getPeriodDate.setVisibility(View.GONE);
			getDate.setVisibility(View.GONE);
		} else if (alarmIds == 3) {
			icon.setImageResource(R.drawable.alerta_para_mamografia_button);
			getPeriodDate.setVisibility(View.GONE);
			getDate.setVisibility(View.GONE);
		} else if (alarmIds == 4) {
			icon.setImageResource(R.drawable.ecography);
			getPeriodDate.setVisibility(View.GONE);
			getDate.setVisibility(View.GONE);
		}
		getDate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				n2 = 2;
				Intent intent = new Intent(SelfExamActivity.this,
						PeriodDateActivity.class);
				intent.putExtra("n", 2);
				startActivityForResult(intent, 1);
			}
		});
		alarm = new SelfExamAlarmReceive();

		getPeriodDate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(SelfExamActivity.this,
						PeriodDateActivity.class);
				n2 = 1;
				intent.putExtra("n", 1);
				startActivityForResult(intent, 1);

			}
		});

		getAlarmDate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// showDialog(DATE_DIALOG_ID);

				if (alarmIds == 2) {
					Intent intent = new Intent(SelfExamActivity.this,
							AppointmentAlarm.class);
					startActivityForResult(intent, 3);
					// intent.putExtra("appointment", 1);
				} else if (alarmIds == 3) {
					Intent intent = new Intent(SelfExamActivity.this,
							MamographyAlert.class);
					// intent.putExtra("appointment", 2);
					startActivityForResult(intent, 4);
				} else if (alarmIds == 4) {
					Intent intent = new Intent(SelfExamActivity.this,
							EcographyAlarmActivity.class);
					// intent.putExtra("appointment", 2);
					startActivityForResult(intent, 6);
				}
			}
		});

		getTime.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(SelfExamActivity.this,
						SetTimeActivity.class);
				intent.putExtra("AlarmId", alarmIds);
				startActivityForResult(intent, 5);

			}
		});
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
	}

	public void setAlert(View v) {
		Context context = getApplicationContext();
		// Log.v("m", " " + message1)
		CharSequence AlarmAlready = getResources().getString(
				R.string.AlarmAlreadySet);
		CharSequence AlarmSet = getResources().getString(R.string.AlarmSet);
		CharSequence AlarmNull = getResources().getString(R.string.Null);
		CharSequence AlarmGet = getResources().getString(
				R.string.SetDateAndTime);
		if (alarmIds == 1) {
			mHourSelf = preference.getInt("nHourSelf", 8);
			mMinuteSelf = preference.getInt("nMinuteSelf", 0);
		} else if (alarmIds == 2) {
			mHourGyna = preference.getInt("nHourGyna", 8);
			mMinuteGyna = preference.getInt("nMinuteGyna", 0);
		} else if (alarmIds == 3) {
			mHourMamo = preference.getInt("nHourMamo", 8);
			mMinuteMamo = preference.getInt("nMinuteMamo", 0);
		} else if (alarmIds == 4) {
			mHourEco = preference.getInt("nHourEco", 8);
			mMinuteEco = preference.getInt("nMinuteEco", 0);
		}
		if (alarmIds == 1) {
			setFirstAlarm(AlarmGet, AlarmSet, AlarmNull, AlarmAlready);
		} else if (alarmIds == 2) {
			setSecondAlarm(AlarmGet, AlarmSet, AlarmNull, AlarmAlready);
		} else if (alarmIds == 3) {
			setThirdAlarm(AlarmGet, AlarmSet, AlarmNull, AlarmAlready);
		} else if (alarmIds == 4) {
			setFourthAlarm(AlarmGet, AlarmSet, AlarmNull, AlarmAlready);
		}
	}

	public void setFirstAlarm(CharSequence AlarmGet, CharSequence AlarmSet,
			CharSequence AlarmNull, CharSequence AlarmAlready) {
		Context context = getApplicationContext();
		String con1 = preference.getString("keySelf", " ");
		Log.v("confirm", con1);
		Log.v("monthly alarm"," "+preference.getInt("alarmType", -1));
		if (con1 == " " || mHourSelf == -1 || mMinuteSelf == -1) {
			Toast.makeText(context, AlarmGet, Toast.LENGTH_SHORT).show();
		}

		else {
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			Date date = new Date();

			try {
				date = dateFormat.parse(selfString);
			} catch (java.text.ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			date.setHours(mHourSelf);
			date.setMinutes(mMinuteSelf);
			Calendar cal = Calendar.getInstance();
			Date alarmDate = new Date();
			cal.setTime(date);
			confirmSelfAlarm1 = " " + con1 + cycleLength + mHourSelf + mMinuteSelf;
			if (confirmSelfAlarm1.equals(preference.getString(confirmSelfAlarm,
					" "))) {
				Toast.makeText(context, AlarmAlready, Toast.LENGTH_SHORT)
						.show();
			} else {

				Log.v("con", confirmSelfAlarm1);
				Editor editor1 = preference.edit();
				editor1.putString(confirmSelfAlarm, confirmSelfAlarm1);
				editor1.commit();
				if (preference.getInt("alarmType", -1) == 1) {
					cal.add(Calendar.DATE, 8);
					//cal.add(Calendar.DATE, cycleLength);
				} else if (preference.getInt("alarmType", -1) == 2) {
					// cal.add(Calendar.MONTH, 1);
				}
				alarmDate = cal.getTime();
				Calendar cal1 = Calendar.getInstance();
				Date currentDate = new Date();
				currentDate = cal1.getTime();
				while (alarmDate.compareTo(currentDate) < 0) {
					if (preference.getInt("alarmType", -1) == 1) {
						cal.add(Calendar.DATE, cycleLength);
					} else if (preference.getInt("alarmType", -1) == 2) {
						cal.add(Calendar.MONTH, 1);
					}
					alarmDate = cal.getTime();
				}
				Editor editor = preference.edit();
				editor.putLong("repeatDateSelf", alarmDate.getTime());
				editor.commit();
				if (alarm != null) {
					AlarmManager am = (AlarmManager) context
							.getSystemService(Context.ALARM_SERVICE);
					Intent intent = new Intent(context,
							SelfExamAlarmReceive.class);
					//int alarmFirst=preference.getInt("alarmType", -1);
					intent.putExtra("alarm", alarmIds);
					intent.putExtra("cycle", cycleLength);
					//intent.putExtra("alarmFirstType", alarmFirst);
					@SuppressWarnings("static-access")
					PendingIntent pi = PendingIntent.getBroadcast(context, 0,
							intent, 0);

					getDuration();
					am.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pi);

					Toast.makeText(context, AlarmSet, Toast.LENGTH_SHORT)
							.show();

				} else {
					Toast.makeText(context, AlarmNull, Toast.LENGTH_SHORT)
							.show();
				}
			}
		}
	}

	public void setSecondAlarm(CharSequence AlarmGet, CharSequence AlarmSet,
			CharSequence AlarmNull, CharSequence AlarmAlready) {
		Context context = getApplicationContext();
		String con2 = preference.getString("keyGyna", " ");
		if (con2 == " " || mHourGyna == -1 || mMinuteGyna == -1) {
			Toast.makeText(context, AlarmGet, Toast.LENGTH_SHORT).show();
		} else {
			// confirmGyna = 0;
			// timeGyna = 0;
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			Date gynedate = new Date();

			try {
				gynedate = dateFormat.parse(appoinment);
			} catch (java.text.ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			gynedate.setHours(mHourGyna);
			gynedate.setMinutes(mMinuteGyna);
			confirmGynaAlarm1 = " " + con2 + mHourGyna + mMinuteGyna;
			if (confirmGynaAlarm1.equals(preference.getString(confirmGynaAlarm,
					" "))) {
				Toast.makeText(context, AlarmAlready, Toast.LENGTH_SHORT)
						.show();
			} else {

				Log.v("con", confirmGynaAlarm1);
				Editor editor1 = preference.edit();
				editor1.putString(confirmGynaAlarm, confirmGynaAlarm1);
				editor1.commit();
				Calendar cal1 = Calendar.getInstance();
				cal1.setTime(gynedate);
				Date alarmDate = new Date();
				Calendar cal2 = Calendar.getInstance();
				Date currentDate = new Date();
				currentDate = cal2.getTime();
				alarmDate = cal1.getTime();
				while (alarmDate.compareTo(currentDate) < 0) {
					cal1.add(Calendar.YEAR, 1);
					alarmDate = cal1.getTime();

				}
				Editor editor = preference.edit();
				editor.putLong("repeatDateGyna", alarmDate.getTime());
				editor.commit();
				Log.v("n", " " + alarmDate);
				if (alarm != null) {
					AlarmManager am1 = (AlarmManager) context
							.getSystemService(Context.ALARM_SERVICE);
					Intent intent = new Intent(context,
							SelfExamAlarmReceive.class);
					// intent.putExtra("sound2", message2);
					intent.putExtra("alarm", alarmIds);
					@SuppressWarnings("static-access")
					PendingIntent pi = PendingIntent.getBroadcast(context, 22,
							intent, 0);
					am1.cancel(pi);

					getApointmentDuration();
					/*
					 * am1.setRepeating(AlarmManager.RTC_WAKEUP,
					 * cal1.getTimeInMillis(), am1.INTERVAL_DAY
					 * apointmentDuration, pi);
					 */
					am1.set(AlarmManager.RTC_WAKEUP, cal1.getTimeInMillis(), pi);
					Toast.makeText(context, AlarmSet, Toast.LENGTH_SHORT)
							.show();
				} else {
					Toast.makeText(context, AlarmNull, Toast.LENGTH_SHORT)
							.show();
				}
			}
		}

	}

	public void setThirdAlarm(CharSequence AlarmGet, CharSequence AlarmSet,
			CharSequence AlarmNull, CharSequence AlarmAlready) {

		Context context = getApplicationContext();
		String con3 = preference.getString("keyMamo", " ");
		if (con3 == " " || mHourMamo == -1 || mMinuteMamo == -1) {
			Toast.makeText(context, AlarmGet, Toast.LENGTH_SHORT).show();

		} else {
			// confirmMamo = 0;
			// timeMamo = 0;
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			Date mamoDate = new Date();
			Date mamoDate1 = new Date();
			Date alarmDate = new Date();
			try {
				mamoDate = dateFormat.parse(mamoDateString);
			} catch (java.text.ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			mamoDate.setHours(mHourMamo);
			mamoDate.setMinutes(mMinuteMamo);
			confirmMamoAlarm1 = " " + con3 + mHourMamo + mMinuteMamo;
			if (confirmMamoAlarm1.equals(preference.getString(
					"confirmMamoAlarm", " "))) {
				Toast.makeText(context, AlarmAlready, Toast.LENGTH_SHORT)
						.show();
				Log.v("con", preference.getString(confirmMamoAlarm, " "));
				Log.v("con1", confirmMamoAlarm1);
			} else {

				Log.v("con", preference.getString(confirmMamoAlarm, " "));
				Log.v("con1", confirmMamoAlarm1);
				Editor editor1 = preference.edit();
				editor1.putString(confirmMamoAlarm, confirmMamoAlarm1);
				editor1.commit();
				Calendar cal1 = Calendar.getInstance();
				cal1.setTime(mamoDate);
				// cal1.add(Calendar.YEAR, 1);
				alarmDate = cal1.getTime();
				Calendar cal2 = Calendar.getInstance();
				Date currentDate = new Date();
				currentDate = cal2.getTime();
				while (alarmDate.compareTo(currentDate) < 0) {
					cal1.add(Calendar.YEAR, 1);
					alarmDate = cal1.getTime();
				}
				Editor editor = preference.edit();
				editor.putLong("repeatDateMamo", alarmDate.getTime());
				editor.commit();
				if (alarm != null) {
					AlarmManager am2 = (AlarmManager) context
							.getSystemService(Context.ALARM_SERVICE);
					Intent intent = new Intent(context,
							SelfExamAlarmReceive.class);
					// intent.putExtra("sound3", message3);
					intent.putExtra("alarm", alarmIds);
					@SuppressWarnings("static-access")
					PendingIntent pi = PendingIntent.getBroadcast(context, 21,
							intent, 0);
					am2.cancel(pi);
					getMamoDuration();
					// am2.setRepeating(AlarmManager.RTC_WAKEUP,
					// cal1.getTimeInMillis(), am2.INTERVAL_DAY
					// * mamoDuration, pi);
					am2.set(AlarmManager.RTC_WAKEUP, cal1.getTimeInMillis(), pi);
					Toast.makeText(context, AlarmSet, Toast.LENGTH_SHORT)
							.show();
				} else {
					Toast.makeText(context, AlarmNull, Toast.LENGTH_SHORT)
							.show();
				}
			}
		}

	}

	public void setFourthAlarm(CharSequence AlarmGet, CharSequence AlarmSet,
			CharSequence AlarmNull, CharSequence AlarmAlready) {
		Context context = getApplicationContext();
		String con4 = preference.getString("keyEco", " ");
		if (con4 == " " || mHourEco == -1 || mMinuteEco == -1) {
			Toast.makeText(context, AlarmGet, Toast.LENGTH_SHORT).show();

		} else {
			// confirmMamo = 0;
			// timeMamo = 0;
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			Date ecoDate = new Date();
			Date alarmDate = new Date();
			try {
				ecoDate = dateFormat.parse(ecoDateString);
			} catch (java.text.ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			ecoDate.setHours(mHourEco);
			ecoDate.setMinutes(mMinuteEco);
			confirmEcoAlarm1 = " " + con4 + mHourEco + mMinuteEco;
			if (confirmEcoAlarm1.equals(preference.getString("confirmEcoAlarm",
					" "))) {
				Toast.makeText(context, AlarmAlready, Toast.LENGTH_SHORT)
						.show();

			} else {

				Log.v("con", preference.getString(confirmMamoAlarm, " "));
				Log.v("con1", confirmMamoAlarm1);
				Editor editor1 = preference.edit();
				editor1.putString(confirmEcoAlarm, confirmEcoAlarm1);
				editor1.commit();
				Calendar cal1 = Calendar.getInstance();
				cal1.setTime(ecoDate);
				// cal1.add(Calendar.YEAR, 1);
				alarmDate = cal1.getTime();
				Calendar cal2 = Calendar.getInstance();
				Date currentDate = new Date();
				currentDate = cal2.getTime();
				while (alarmDate.compareTo(currentDate) < 0) {
					cal1.add(Calendar.YEAR, 1);
					alarmDate = cal1.getTime();
				}
				Editor editor = preference.edit();
				editor.putLong("repeatDateEco", alarmDate.getTime());
				editor.commit();
				if (alarm != null) {
					AlarmManager am2 = (AlarmManager) context
							.getSystemService(Context.ALARM_SERVICE);
					Intent intent = new Intent(context,
							SelfExamAlarmReceive.class);
					// intent.putExtra("sound3", message3);
					intent.putExtra("alarm", alarmIds);
					@SuppressWarnings("static-access")
					PendingIntent pi = PendingIntent.getBroadcast(context, -1,
							intent, 0);
					am2.cancel(pi);
					getEcoDuration();
					// am2.setRepeating(AlarmManager.RTC_WAKEUP,
					// cal1.getTimeInMillis(), am2.INTERVAL_DAY
					// * ecoDuration, pi);
					am2.set(AlarmManager.RTC_WAKEUP, cal1.getTimeInMillis(), pi);
					Toast.makeText(context, AlarmSet, Toast.LENGTH_SHORT)
							.show();
				} else {
					Toast.makeText(context, AlarmNull, Toast.LENGTH_SHORT)
							.show();
				}
			}
		}

	}

	@Deprecated
	private long getDuration() {

		Calendar cal1 = Calendar.getInstance();
		int currentMonth = cal1.get(Calendar.MONTH);
		currentMonth++;
		if (currentMonth > Calendar.DECEMBER) {
			currentMonth = Calendar.JANUARY;
			cal1.set(Calendar.YEAR, cal1.get(Calendar.YEAR) + 1);
		}
		cal1.set(Calendar.MONTH, currentMonth);
		int maximumDay = cal1.getActualMaximum(Calendar.DAY_OF_MONTH);

		duration = maximumDay;
		return duration;
	}

	private long getApointmentDuration() {
		Calendar cal1 = Calendar.getInstance();
		// cal1.add(Calendar.YEAR,1);
		int noOfDays = cal1.getActualMaximum(Calendar.DAY_OF_YEAR);
		apointmentDuration = noOfDays;
		return apointmentDuration;

	}

	private long getMamoDuration() {
		Calendar cal1 = Calendar.getInstance();
		// cal1.add(Calendar.YEAR,1);
		int noOfDays = cal1.getActualMaximum(Calendar.DAY_OF_YEAR);
		mamoDuration = noOfDays;
		return mamoDuration;

	}

	private long getEcoDuration() {
		Calendar cal1 = Calendar.getInstance();
		// cal1.add(Calendar.YEAR,1);
		int noOfDays = cal1.getActualMaximum(Calendar.DAY_OF_YEAR);
		ecoDuration = noOfDays;
		return ecoDuration;

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == 1) {
			if (resultCode == RESULT_OK) {
				selfString = data.getStringExtra("selfDate");
				cycleLength = data.getIntExtra("cycle", 0);
				// confirmSelfExam = data.getIntExtra("confirm", 0);
			}
		} else if (requestCode == 2) {
			if (resultCode == RESULT_OK) {
				sound = data.getIntExtra("sound_path", 0);
			}
		} else if (requestCode == 3) {
			if (resultCode == RESULT_OK) {

				appoinment = data.getStringExtra("appoinment");
				// confirmGyna = data.getIntExtra("confirmGyna", 0);
			}
		} else if (requestCode == 4) {
			if (resultCode == RESULT_OK) {
				mamoDateString = data.getStringExtra("mamoDateString");
				// confirmMamo = data.getIntExtra("confirmMamo", 0);
			}
		} else if (requestCode == 5) {
			if (resultCode == RESULT_OK) {
				if (alarmIds == 1) {
					// mHourSelf =data.getIntExtra("mHourSelf",-1);
					// mMinuteSelf =data.getIntExtra("mMinuteSelf",-1);
					// timeSelf = data.getIntExtra("timeSelf", 0);
				} else if (alarmIds == 2) {
					// mHourGyna = data.getIntExtra("mHourGyna",-1);
					// mMinuteGyna = data.getIntExtra("mMinuteGyna",-1);
					// timeGyna = data.getIntExtra("timeGyna", 0);
				} else if (alarmIds == 3) {
					// mHourMamo =data.getIntExtra("mHourMamo",-1);
					// mMinuteMamo =data.getIntExtra("mMinuteMamo",-1);
					// timeMamo = data.getIntExtra("timeMamo", 0);
				} else if (alarmIds == 4) {
					// mHourMamo =data.getIntExtra("mHourMamo",-1);
					// mMinuteMamo =data.getIntExtra("mMinuteMamo",-1);
					// timeEco = data.getIntExtra("timeEco", 0);
				}
			}
		} else if (requestCode == 6) {
			if (resultCode == RESULT_OK) {
				if (resultCode == RESULT_OK) {
					ecoDateString = data.getStringExtra("ecoDateString");
					// confirmEco = data.getIntExtra("confirmeco", 0);
				}
			}
		}
	}
}
